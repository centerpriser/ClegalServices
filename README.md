To install the project on your local machine please do the following:

1. Clone the gitLab project into your eclipse workspace
2. Clone URL : https://gitlab.com/centerpriser/ClegalServices.git
3. Insert UserName/Password … already shared with you.
4. Once you've successfully cloned the project, open the gitRepository view and directly import project into your workspace:
![Alt text](/-/blob/master/readme_images/Screenshot_2021-08-27_at_08.07.13.png/raw=true "Import Maven Project")
5. Once you've successfully imported the project into your workspace, update Maven.
![Alt text](https://gitlab.com/centerpriser/ClegalServices/-/blob/master/readme_images/Screenshot_2021-08-27_at_08.07.20.png/rwa=true "Maven Update")
6. Once updated do the refresh and clean your project.
7. Before run the project ensures that Mongodb installed and running into your system.
8. To run the project do the below setting using your debug configuration.
![Alt text](https://gitlab.com/centerpriser/ClegalServices/-/blob/master/readme_images/Screenshot_2021-08-27_at_08.07.37.png/raw=true "Maven Update")
9. If all went well then then it will start your project.
10. Currently you can login to our application using the below url:
http://localhost:8080/
http://localhost:8080/eadmin

