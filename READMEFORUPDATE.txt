=== 11-Feb== 2018
PLEASE ensure that your collection name is userMaster (not usermaster) in your clegal db.
Create the index on userMaster collection before executing the code:
command to create index on userMaster collection: 
db.userMaster.createIndex( { address: "text" } )


===9 Mar 2018=====


Remove the old userMaster table to replace it with the new crawled data.

db.userMaster.renameCollection("userMasterOld");

db.usermaster_temp.renameCollection("userMaster");

Create Index for the table again:
db.userMaster.createIndex( { address: "text" } );
