import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    def start_requests(self):
        start_urls = [
                'https://www.mon-avocat.fr/trouver/avocats/dijon/droit-de-la-famille-des-personnes-et-de-leur-patrimoine'
            ]
#         citiesInFrance = ['Abbeville', 'Agde', 'Agen', 'Aix-en-Provence', 'Aix-les-Bains', 'Ajaccio', 'Albi', 'Alençon', 'Alfortville', 'Alès', 'Amiens', 'Angers', 'Anglet', 'Angoulême', 'Annecy', 'Annemasse', 'Antibes', 'Juan-les-Pins', 'Antony', 'Argenteuil', 'Arles', 'Armentières', 'Arras', 'Asnières-sur-Seine', 'Athis-Mons', 'Aubagne', 'Aubervilliers', 'Auch', 'Aulnay-sous-Bois', 'Aurillac', 'Auxerre', 'Avignon', 'Bagneux', 'Bagnolet', 'Bastia', 'Bayonne', 'Beaune', 'Beauvais', 'Belfort', 'Bergerac', 'Besançon', 'Bezons', 'Biarritz', 'Blagnac', 'Blois', 'Bobigny', 'Bois-Colombes', 'Bordeaux', 'Boulogne-Billancourt', 'Boulogne-sur-Mer', 'Bourg-en-Bresse', 'Bourges', 'Bourgoin-Jallieu', 'Brest', 'Brive-la-Gaillarde', 'Bron', 'Bruay-la-Buissière', 'Brunoy', 'Brétigny-sur-Orge', 'Bègles', 'Béthune', 'Béziers', 'Cachan', 'Caen', 'Cagnes-sur-Mer', 'Cahors', 'Calais', 'Caluire-et-Cuire', 'Cambrai', 'Cannes', 'Carcassonne', 'Carpentras', 'Castres', 'Cavaillon', 'Cayenne', 'Cenon', 'Cergy', 'Chalon-sur-Saône', 'Chambéry', 'Champigny-sur-Marne', 'Champs-sur-Marne', 'Charenton-le-Pont', 'Charleville-Mézières', 'Chartres', 'Chatou', 'Chaumont', 'Chelles', 'Cherbourg-Octeville', 'Choisy-le-Roi', 'Châlons-en-Champagne', 'Châteauroux', 'Châtellerault', 'Châtenay-Malabry', 'Châtillon', 'Clamart', 'Clermont-Ferrand', 'Clichy', 'Clichy-sous-Bois', 'Colmar', 'Colombes', 'Colomiers', 'Combs-la-Ville', 'Compiègne', 'Conflans-Sainte-Honorine', 'Corbeil-Essonnes', 'Coudekerque-Branche', 'Courbevoie', 'Creil', 'Croix', 'Créteil', 'Dammarie-lès-Lys', 'Denain', 'Deuil-la-Barre', 'Dieppe', 'Dijon', 'Dole', 'Douai', 'Draguignan', 'Drancy', 'Draveil', 'Dunkerque', 'Décines-Charpieu', 'Eaubonne', 'Ermont', 'Fleury-les-Aubrais', 'Fontaine', 'Fontenay-aux-Roses', 'Fontenay-sous-Bois', 'Forbach', 'Fougères', 'Franconville', 'Fresnes', 'Frontignan', 'Fréjus', 'Fécamp', 'Gagny', 'Gap', 'Garges-lès-Gonesse', 'Gennevilliers', 'Gif-sur-Yvette', 'Gonesse', 'Goussainville', 'Gradignan', 'Grande-Synthe', 'Grasse', 'Grenoble', 'Grigny', 'Guyancourt', 'Haguenau', 'Hazebrouck', 'Herblay', 'Houilles', 'Hyères', 'Hénin-Beaumont', 'Hérouville-Saint-Clair', 'Illkirch-Graffenstaden', 'Issy-les-Moulineaux', 'Istres', 'Ivry-sur-Seine', 'Joué-lès-Tours', 'La Celle Saint-Cloud', 'La Ciotat', 'La Courneuve', 'La Garde', 'La Garenne-Colombes', 'La Madeleine', 'La Possession', 'La Roche-sur-Yon', 'La Rochelle', 'La Seyne-sur-Mer', 'La Teste-de-Buch', 'La Valette-du-Var', 'Lambersart', 'Lanester', 'Laon', 'Laval', 'Le Blanc-Mesnil', 'Le Bouscat', 'Le Cannet', 'Le Chesnay', 'Le Creusot', 'Le Grand-Quevilly', 'Le Havre', 'Le Kremlin-Bicêtre', 'Le Mans', 'Le Mée-sur-Seine', 'Le Perreux-sur-Marne', 'Le Petit-Quevilly', 'Le Plessis-Robinson', 'Le Port', 'Le Tampon', 'Lens', 'Les Lilas', 'Les Ulis', 'Levallois-Perret', 'Libourne', 'Lille', 'Limoges', 'Lisieux', 'Livry-Gargan', 'Liévin', 'Longjumeau', 'Loos', 'Lorient', 'Lormont', 'Lunel', 'Lunéville', 'Lyon', 'Maisons-Alfort', 'Maisons-Laffitte', 'Malakoff', 'Manosque', 'Mantes-la-Jolie', 'Marcq-en-Baroeul', 'Marignane', 'Marseille', 'Martigues', 'Massy', 'Maubeuge', 'Meaux', 'Melun', 'Menton', 'Metz', 'Meudon', 'Meyzieu', 'Millau', 'Miramas', 'Mons-en-Baroeul', 'Mont-Saint-Aignan', 'Mont-de-Marsan', 'Montauban', 'Montbéliard', 'Montfermeil', 'Montgeron', 'Montigny-le-Bretonneux', 'Montigny-lès-Metz', 'Montluçon', 'Montmorency', 'Montpellier', 'Montreuil', 'Montrouge', 'Montélimar', 'Moulins', 'Muret', 'Mâcon', 'Mérignac', 'Nancy', 'Nanterre', 'Nantes', 'Narbonne', 'Neuilly-sur-Marne', 'Neuilly-sur-Seine', 'Nevers', 'Nice', 'Niort', 'Nogent-sur-Marne', 'Noisy-le-Grand', 'Noisy-le-Sec', 'Nîmes', 'Olivet', 'Orange', 'Orly', 'Orléans', 'Orvault', 'Oullins', 'Oyonnax', 'Ozoir-la-Ferrière', 'Pantin', 'Paris', 'Pau', 'Pessac', 'Pierrefitte-sur-Seine', 'Plaisir', 'Poissy', 'Poitiers', 'Pontault-Combault', 'Pontoise', 'Puteaux', 'Périgueux', 'Quimper', 'Reims', 'Rennes', 'Rezé', 'Rillieux-la-Pape', 'Ris-Orangis', 'Roanne', 'Rochefort', 'Rodez', 'Romainville', 'Romans-sur-Isère', 'Rosny-sous-Bois', 'Roubaix', 'Rouen', 'Rueil-Malmaison', 'Saint-André', 'Saint-Benoît', 'Saint-Brieuc', 'Saint-Chamond', 'Saint-Cloud', 'Saint-Denis', 'Saint-Dizier', 'Saint-Dié-des-Vosges', 'Saint-Germain-en-Laye', 'Saint-Herblain', 'Saint-Joseph', 'Saint-Laurent-du-Var', 'Saint-Leu', 'Saint-Louis', 'Saint-Malo', 'Saint-Martin-d"Hères', 'Saint-Maur-des-Fossés', 'Saint-Michel-sur-Orge', 'Saint-Médard-en-Jalles', 'Saint-Nazaire', 'Saint-Ouen', 'Saint-Paul', 'Saint-Pierre', 'Saint-Pol-sur-Mer', 'Saint-Priest', 'Saint-Quentin', 'Saint-Raphaël', 'Saint-Sébastien-sur-Loire', 'Saint-Étienne', 'Saint-Étienne-du-Rouvray', 'Sainte-Foy-lès-Lyon', 'Sainte-Geneviève-des-Bois', 'Saintes', 'Salon-de-Provence', 'Sannois', 'Sarcelles', 'Sarreguemines', 'Sartrouville', 'Saumur', 'Savigny-le-Temple', 'Savigny-sur-Orge', 'Schiltigheim', 'Sedan', 'Sens', 'Sevran', 'Six-Fours-les-Plages', 'Soissons', 'Sotteville-lès-Rouen', 'Stains', 'Strasbourg', 'Sucy-en-Brie, Suresnes', 'Sète', 'Sèvres', 'Talence', 'Tarbes', 'Taverny', 'Thiais', 'Thionville', 'Thonon-les-Bains', 'Torcy', 'Toulon', 'Toulouse', 'Tourcoing', 'Tournefeuille', 'Tours', 'Trappes', 'Tremblay-en-France', 'Troyes', 'Valence', 'Valenciennes', 'Vallauris', 'Vandoeuvre-lès-Nancy', 'Vannes', 'Vanves', 'Vaulx-en-Velin', 'Vernon', 'Versailles', 'Vertou', 'Vichy', 'Vierzon', 'Vigneux-sur-Seine', 'Villefranche-sur-Saône', 'Villejuif', 'Villemomble', "Villenave-d'Ornon", 'Villeneuve-Saint-Georges', "Villeneuve-d'Ascq", 'Villeneuve-la-Garenne', 'Villeneuve-sur-Lot', 'Villeparisis', 'Villepinte', 'Villeurbanne', 'Villiers-le-Bel', 'Villiers-sur-Marne', 'Vincennes', 'Viry-Châtillon', 'Vitrolles', 'Vitry-sur-Seine', 'Voiron', 'Vélizy-Villacoublay', 'Vénissieux', 'Wattrelos', 'Yerres', 'Échirolles', 'Élancourt', 'Épernay', 'Épinal', 'Épinay-sur-Seine', 'Étampes', 'Évreux', 'Évry']
        domainsOfExpertise = ['droit-de-la-famille-des-personnes-et-de-leur-patrimoine', 'droit-penal', 'divorce', 'droit-du-travail', 'droit-civil', 'droit-de-limmobilier', 'droit-commercial-et-de-la-concurrence', 'droit-des-affaires', 'droit-du-dommage-corporel', 'droit-des-contrats', 'droit-social', 'droit-des-societes', 'droit-de-la-construction', 'contentieux', 'droit-de-la-responsabilite-civile', 'droit-du-credit-et-de-la-consommation', 'droit-locatif', 'droit-fiscal', 'droit-bancaire-et-boursier', 'droit-des-assurances', 'droit-des-mineurs', 'droit-administratif', 'droit-public', 'droit-routier', 'droit-de-la-securite-sociale-et-de-la-protection-sociale', 'droit-des-etrangers-et-de-la-nationalite', 'droit-de-lurbanisme', 'droit-de-la-propriete-intellectuelle', 'droit-des-successions', 'droit-de-la-securite-sociale', 'baux-commerciaux', 'baux-dhabitation', 'droit-de-la-sante', 'responsabilite-medicale-et-indemnisation-des-victimes', 'procedures-civiles-dexecution', 'conseil-des-prudhommes', 'procedures-collectives', 'mediation', 'procedure-dappel', 'droit-des-victimes', 'droit-de-la-copropriete', 'accident-du-travail', 'licenciement', 'contrats-de-travail', 'droit-rural', 'droit-de-la-fonction-publique', '185-indemnisation-du-prejudice', 'droit-des-garanties-des-suretes-et-des-mesures-dexecution', 'droit-du-numerique', 'harcelement-au-travail', '182-reparation-du-prejudice', 'tribunal-des-affaires-de-securite-sociale', 'droit-de-lenvironnement', 'droit-international-et-de-lunion-europeenne', 'droit-penal-des-affaires', 'droit-de-la-propriete-industrielle', 'contentieux-fiscal', 'droit-des-marches-publics', 'droit-des-associations-et-des-fondations', 'droit-dauteur', 'droit-de-la-presse', 'droit-de-lexpropriation', 'droit-de-lautomobile', 'droit-public-economique', 'contrat-public', '181-recouvrement-de-creances', 'droit-du-sport', 'droit-de-la-saisie-immobiliere', 'droit-des-biens', 'droit-prive', 'transmission-et-restructuration-dentreprises', 'droit-de-la-concurrence', '196-fond-de-commerce', 'patrimoine', 'maladie-professionnelle', 'encheres', 'droit-des-collectivites-locales', 'droit-syndical', 'tribunal-du-contentieux-de-lincapacite', 'droit-de-larbitrage', 'heures-supplementaires', 'commandes-publiques', 'droit-douanier', 'droit-des-marques', 'droit-des-transports', 'mandataire-immobilier', 'tutelle', 'pret-bancaire', 'droit-de-la-propriete-litteraire-et-artistique', 'droit-de-la-distribution', 'pacs', 'faillites-et-entreprises-en-difficulte', 'droit-equin', 'bail-professionnel', 'droit-a-limage', 'droit-de-lenergie', 'rupture-conventionnelle', 'clause-de-non-concurrence', 'droit-de-la-nationalite', 'paie-recrutement-formation-et-conseil-en-ressources-humaines', 'droit-de-le-commerce', 'droit-maritime', 'droit-representant-du-personnel', 'conflits-entre-associes', 'concurrence-deloyale', 'parasitisme-commercial', 'droit-notarial', 'droit-des-mobilites', '189-droit-des-affaires-internationales', '187-recouvrement-de-creances', '198-conflit-de-voisinage', 'inaptitude-physique-du-salarie', 'prestations-sociales', 'droit-de-lamenagement-du-territoire', 'droits-de-lhomme', 'arbitrage', 'droit-de-la-retraite', 'droit-economique', 'contentieux-du-risque-professionnel', 'droit-de-la-chasse', 'droit-de-la-securite-privee', 'droit-de-la-plaisance-et-du-nautisme', 'droit-penal-international', '195-permis-de-conduire', '202-droits-lgbt', '197-droit-de-la-franchise', 'droit-pharmaceutique', 'negociation', '192-droit-electoral', 'protection-des-donnees-personnelles', 'droit-local', 'droit-franco-allemand', 'urssaf', 'droit-de-la-publicite', 'vices-caches', 'contrat-de-rd', 'invention-des-salaries', 'droit-de-la-regulation', 'penalites-financieres', 'clause-de-confidentialite', '191-droit-du-handicap', 'retraite', 'secretariat-juridique', '183-expertise-medicale', '188-conflits-entre-associes', '200-droit-des-chomeurs', 'statut-dauto-entrepreneur', 'allocation-supplementaire', 'reglementation-des-drones', 'protection-du-savoir-faire-en-innovation', 'exclusivite-dactivite', '194-strategie-patrimoniale-des-familles-recomposees', '193-droit-du-transport-aerien', 'droit-de-la-formation-professionnelle', '190-erreur-teg', 'droit-des-jeux', 'droit-commercial-et-de-la-concurrence']
        citiesInFrance = ['france']
#         domainsOfExpertise = ['droit-penal']
        for i in citiesInFrance:
            for j in domainsOfExpertise:
                url = 'https://www.mon-avocat.fr/trouver/avocats/'+i.lower()+'/'+j
#         for url in start_urls:
                yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()
#         for result in response.css('div.tile.material-shadow-1'):
#             for href in response.css('div.tile-title a::attr(href)'):
#                 yield {
#                         'title' : result.css('div.tile-title a::text').extract(),
#                         'status' : result.css('div.tile-status::text').extract(),
#                         'geo' : result.css('div.tile-geo a::text').extract(),
#                         'address' : result.css('div.tile-address::text').extract(),    
#                         'domain' : result.css('div.tile-domain::text').extract(),    
#                         'list' : result.css('i.fa fa-check::text').extract(),
#                         'more info': result.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)').extract(),
#                         'telephone': href.css(response.follow(href, self.parse_author))            
#                         }
#                 for href in response.css('div.tile-title a::attr(href)'):
#                     yield response.follow(href, self.parse_author)
        for href in response.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)'):
            yield response.follow(href, self.parse_author)
            
#         next_page = response.css('ul.pagination.short li a::attr(href)').extract_first()
#         print('next page', next_page)
#         if next_page is not None:
#             next_page = response.urljoin(next_page)
#             yield scrapy.Request(next_page, callback=self.parse)

        # follow pagination links
        for href in response.css('ul.pagination.short li a::attr(href)'):
            print('next page', href)
            yield response.follow(href, self.parse)

            
    def parse_author(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        yield {
            'name': extract_with_css('h1::text'),   
            'telephone': extract_with_css('span.highlight a::text'),
            'status': extract_with_css('div.m-profile-status::text'),
            'specialities': extract_with_css('div.m-profile-domain a::text'),
            'payment': response.css('div.hidden-xs ul.m-profile-tag li::text').extract(),
            'languages': response.css('div.hidden-xs ul.m-profile-formation li::text').extract(),
            'education': response.css('div.hidden-xs div p::text').extract(),
            'description': response.css('div.col-sm-8 p::text').extract_first(),
            'address': response.css('div.m-profile-address.hidden-xs span::text').extract(),
            'timingsDay': response.css('div.m-profile-date table tbody tr td.day::text').extract(),
            'timingsHour': response.css('div.m-profile-date table tbody tr td.hour::text').extract(),
            'toqueNumero': response.css('div.m-profile.material-shadow-1 p.m-profile-more-data::text').extract(),
            'payment': response.css('div.m-profile.material-shadow-1 p.m-profile-more-data span::text').extract(),
            'lawyerSite':response.css('div.text-center a.bttn.bttn-blue.bttn-sm.bttn-faIcon::attr(href)').extract()
        }

            
