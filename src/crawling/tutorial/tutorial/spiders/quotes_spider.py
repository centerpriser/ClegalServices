import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    def start_requests(self):
        start_urls = [
                'https://www.mon-avocat.fr/trouver/avocats'
            ]
        citiesInFrance = ['75000-paris-france']
        domainsOfExpertise = ['divorce']
        for i in citiesInFrance:
            for j in domainsOfExpertise:
                url = 'https://www.mon-avocat.fr/trouver/avocats/'+i.lower()+'/'+j
#         for url in start_urls:
                yield scrapy.Request(url=url, callback=self.parse)

                


    def parse(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()
        for result in response.css('div.tile.material-shadow-1'):
            for href in response.css('div.tile-title a::attr(href)'):
                yield {
                        'title' : result.css('div.tile-title a::text').extract(),
                        'status' : result.css('div.tile-status::text').extract(),
                        'geo' : result.css('div.tile-geo a::text').extract(),
                        'address' : result.css('div.tile-address::text').extract(),    
                        'domain' : result.css('div.tile-domain::text').extract(),    
                        'list' : result.css('i.fa fa-check::text').extract(),
                        'more info': result.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)').extract()
#                         'telephone': response.follow(href.css('span.highlight a::text')[0])            
                        }
#                 for href in response.css('div.tile-title a::attr(href)'):
#                     yield response.follow(href, self.parse_author)
            
        next_page = response.css('li a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)


            
    def parse_author(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        yield {
            'telephone': extract_with_css('span.highlight a::text'),
        }

            
