import scrapy


class AuthorSpider(scrapy.Spider):
    name = 'telephone'

    start_urls = ['https://www.mon-avocat.fr/trouver/avocats/grenoble/droit-commercial-et-de-la-concurrence']

    def parse(self, response):
        # follow links to author pages
        for href in response.css('div.tile-title a::attr(href)'):
            yield response.follow(href, self.parse_author)

        # follow pagination links
        for href in response.css('li.next a::attr(href)'):
            yield response.follow(href, self.parse)

    def parse_author(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        yield {
            'telephone': extract_with_css('span.highlight a::text'),
        }




