import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    def start_requests(self):
        start_urls = [
                'https://www.courdecassation.fr/jurisprudence_2/arrets_classes_rubriques_2987/'
            ]
        for url in start_urls:yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
#         def extract_with_css(query):
#             return response.css(query).extract_first().strip()
#         for result in response.css('div.tile.material-shadow-1'):
#             for href in response.css('div.tile-title a::attr(href)'):
#                 yield {
#                         'title' : result.css('div.tile-title a::text').extract(),
#                         'status' : result.css('div.tile-status::text').extract(),
#                         'geo' : result.css('div.tile-geo a::text').extract(),
#                         'address' : result.css('div.tile-address::text').extract(),    
#                         'domain' : result.css('div.tile-domain::text').extract(),    
#                         'list' : result.css('i.fa fa-check::text').extract(),
#                         'more info': result.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)').extract(),
#                         'telephone': href.css(response.follow(href, self.parse_author))            
#                         }
#                 for href in response.css('div.tile-title a::attr(href)'):
#                     yield response.follow(href, self.parse_author)
        for href in response.css('ul.mktree2 ul.toggle_container li a::attr(href)'):
            yield response.follow(href, self.parse_author)
            
#         next_page = response.css('ul.pagination.short li a::attr(href)').extract_first()
#         print('next page', next_page)
#         if next_page is not None:
#             next_page = response.urljoin(next_page)
#             yield scrapy.Request(next_page, callback=self.parse)

        # follow pagination links
#         for href in response.css('ul.pagination.short li a::attr(href)'):
#             print('next page', href)
#             yield response.follow(href, self.parse)
# 
#             
    def parse_author(self, response):
        def extract_with_css(query):
            return response.css(query).extract()
        for href in response.css('div.contenu.col-md-9.col-sm-9.col-xs-12'):
            yield {
                'h1': extract_with_css('h1::text'),
                'h2': extract_with_css('h2::text'), 
                'pem': extract_with_css('p em::text'),
                'text': extract_with_css('p::text'),                
                'pStrong': extract_with_css('p strong::text'),
                'pStrongU': extract_with_css('p strong u::text')                                                                               
            }

            
