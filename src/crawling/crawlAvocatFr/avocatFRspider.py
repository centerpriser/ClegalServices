import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    def start_requests(self):
        start_urls = [
                'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=1&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language=',
                'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=2&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language=',
                'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=3&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language=',
                'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=4&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language=',
                'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=5&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language='
            ]
        url = 'https://consultation.avocat.fr/avocats/liste.php?consultingclass=&page=1&source=search&namesearch=0&competencepublic1=&competencepublic2=&name=&address=grenoble&language='
        for url in start_urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()
#         for result in response.css('div.tile.material-shadow-1'):
#             for href in response.css('div.tile-title a::attr(href)'):
#                 yield {
#                         'title' : result.css('div.tile-title a::text').extract(),
#                         'status' : result.css('div.tile-status::text').extract(),
#                         'geo' : result.css('div.tile-geo a::text').extract(),
#                         'address' : result.css('div.tile-address::text').extract(),    
#                         'domain' : result.css('div.tile-domain::text').extract(),    
#                         'list' : result.css('i.fa fa-check::text').extract(),
#                         'more info': result.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)').extract(),
#                         'telephone': href.css(response.follow(href, self.parse_author))            
#                         }
#                 for href in response.css('div.tile-title a::attr(href)'):
#                     yield response.follow(href, self.parse_author)
        for href in response.css('div.profils-wrapper div.profil a.lawyerprofilebox.user-searchresult-wrapper.txtcol::attr(href)').extract():
#             print('href', href)
            yield response.follow(href, self.parse_author)            
#             urls = href.css('div.profil a.lawyerprofilebox.user-searchresult-wrapper.txtcol::attr(href)').extract()
#         print(dict(urls=urls))
#             yield{
#                   'url':extract_with_css('div.profils-wrapper div.profil')
#                   }
            
#         next_page = response.css('ul.pagination.short li a::attr(href)').extract_first()
#         print('next page', next_page)
#         if next_page is not None:
#             next_page = response.urljoin(next_page)
#             yield scrapy.Request(next_page, callback=self.parse)

#         # follow pagination links
#         for href in response.css('ul.pagination.short li a::attr(href)'):
#             print('next page', href)
#             yield response.follow(href, self.parse)

            
    def parse_author(self, response):
#         print('url of page is '+response.request.url)
        def extract_with_css(query):
            if(response.css(query)):
                return response.css(query).extract_first().strip()
#         for detailedCompetences in response.css('div.middlecontent section.competences'):
#             yield{
#                   'title': detailedCompetences.css('div.competence-wrapper div.text-wrapper h3::text').extract_first(),
#                   'competence': detailedCompetences.css('div.competence-wrapper div.text-wrapper div.text::text').extract()
#                   }
            
            
        yield {
            'name': extract_with_css('h1.name a span::text'),
            'address': extract_with_css('div.line-wrapper span.val::text'),
            'languages': extract_with_css('div.line-wrapper.languages span.val::text'),
            'aj': extract_with_css('div.line-wrapper.aj::text'),
            'presentation': extract_with_css('div.seemoresimple-wrapper::text'),
            'domains': response.css('div.competences-wrapper div.competence-wrapper.toggler-simple-wrapper a.name-wrapper.txtcol div.name::text').extract(),
            'detailedCompetences': response.css('div.middlecontent section.competences div.competence-wrapper div.text-wrapper').extract()
        }



            
