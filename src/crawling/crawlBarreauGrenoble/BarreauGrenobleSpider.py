import scrapy


class QuotesSpider(scrapy.Spider):
    name = "BarreauGrenoble"
    def start_requests(self):
        start_urls = [
                'https://www.ordre-grenoble.avocat.fr/avocat/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/2/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/3/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/4/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/5/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/6/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/7/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/8/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/9/?srcq=1',
                'https://www.ordre-grenoble.avocat.fr/avocat/page/10/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/11/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/12/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/13/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/14/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/15/?srcq=1',
                 'https://www.ordre-grenoble.avocat.fr/avocat/page/16/?srcq=1'
            ]
        for url in start_urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
#         def extract_with_css(query):
#             return response.css(query).extract_first().strip()
#         for result in response.css('div.tile.material-shadow-1'):
#             for href in response.css('div.tile-title a::attr(href)'):
#                 yield {
#                         'title' : result.css('div.tile-title a::text').extract(),
#                         'status' : result.css('div.tile-status::text').extract(),
#                         'geo' : result.css('div.tile-geo a::text').extract(),
#                         'address' : result.css('div.tile-address::text').extract(),    
#                         'domain' : result.css('div.tile-domain::text').extract(),    
#                         'list' : result.css('i.fa fa-check::text').extract(),
#                         'more info': result.css('a.bttn.bttn-blue.bttn-faIcon.float-right::attr(href)').extract(),
#                         'telephone': href.css(response.follow(href, self.parse_author))            
#                         }
#                 for href in response.css('div.tile-title a::attr(href)'):
#                     yield response.follow(href, self.parse_author)
#         for href in response.css('div.id-avocat.bloc-clic + div.infos p.titre a.clic::text'):
        for href in response.css('div.id-avocat.bloc-clic'):
#             yield scrapy.Request(url=href.extract(), callback=self.parse_author)
#             yield response.follow(href.css('p.titre a.clic::text').extract(), self.parse_author)
            next_page = href.css('a.clic::attr(href)').extract_first()
            next_page = response.urljoin(next_page)
#             yield{
#                     'address' : href.css('p::text').extract(),
#                     'url' : scrapy.Request(next_page, callback=self.parse_author)
# #                     'other' : response.follow(href.css('a.clic::attr(href)').extract(), self.parse_author)                
#                   }

            yield scrapy.Request(next_page, callback=self.parse_author, meta={'address' : href.css('p::text').extract()})
            
#         next_page = response.css('ul.pagination.short li a::attr(href)').extract_first()
#         print('next page', next_page)
#         if next_page is not None:
#             next_page = response.urljoin(next_page)
#             yield scrapy.Request(next_page, callback=self.parse)

        # follow pagination links
#         for href in response.css('div.infos p.titre a::attr(href)'):
#             print('next page', href)
#             yield response.follow(href, self.parse_author)

            
    def parse_author(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        yield {
            'name' : response.css('h1::text').extract(),
            'email': response.css('div.container.template-blog.template-single-blog p a::text').extract(),
            'address': response.meta['address']   
        }

            
