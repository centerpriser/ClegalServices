import json
import bcrypt
from random import choice

with open('/Users/ankujarora1/git/ClegalServices/src/crawling/crawlBarreauGrenoble/BarreauGrenoblInfo3.json','r') as readFile:
    dataStore = json.load(readFile)
    newList = []
    for i in dataStore:
        dict1 = dict()
        for j in (i['email']):
            if('@' in j):
                dict1['email'] = j        
                splitName = i["name"][0].split()
                dict1['firstname'] = splitName[1].strip()
                dict1['lastname'] = splitName[2:len(splitName)][0]
                dict1['username'] = dict1['firstname'] + '.' + dict1['lastname']
                stringPassword = (''.join([choice('abcdefghijklmnopqrstuvwxyz0123456789%^*(-_=+)') for i in range(8)])).encode('utf-8')
                dict1['password'] = (bcrypt.hashpw(stringPassword, bcrypt.gensalt(rounds = 10, prefix=b"2a"))).decode('utf-8')
                print(dict1['username'])
                print(stringPassword)
#                 dict1['password'] = '$2a$10$MI74irg.bbobBLtHiwO6QuHH5rM9ibwLwTLXQgx.b1zjFWA/uPS/6'
#                 dict1['password'] = (''.join([choice('abcdefghijklmnopqrstuvwxyz0123456789%^*(-_=+)') for i in range(8)]))
                dict1['isLawyer'] = True
                dict1['accountNonExpired'] = False
                dict1['accountNonLocked'] = False
                dict1['credentialsNonExpired'] = False
                dict1['tempPassword'] = True
                dict1['enabled'] = True
                dict1['status'] = 'STATUS_APPROVED'
                dict1['roles'] = ["ROLE_BU"] 
                dict1['address'] = i['address']                                                               
                newList.append(dict1)
    readFile.close()
with open('/Users/ankujarora1/git/ClegalServices/src/crawling/crawlBarreauGrenoble/write16jun.json','w') as writeFile:
    dataStore = json.dump(newList, writeFile, indent=2)

