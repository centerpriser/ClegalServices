package com.clegal.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.clegal.model.ProjectMaster;
import com.clegal.model.TaskMaster;
import com.clegal.repository.ProjectMasterRepository;
import com.clegal.wrapper.ProjectCreationForm;
/**CRUD operations on the ProjectMaster table*/
@Service
public class ProjectMasterSevice  implements IProjectMasterService{

	@Autowired private ProjectMasterRepository projectMasterRepository;


	@Autowired private MongoOperations mongoOperations;

	@Override
	public List<ProjectMaster> findallProjectByID(String searchLawyer) {
		// TODO Auto-generated method stub
		return projectMasterRepository.findProjectsByUserName(searchLawyer);
	}

	@Override
	public void save(ProjectMaster project) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String name = auth.getName(); //get logged in username
	    project.setBuMemberID(name);
		projectMasterRepository.save(project);
	}

	@Override
	public ArrayList<String> deleteProject(String projectID) {
		ProjectMaster tempMaster = projectMasterRepository.findOne(projectID);
		projectMasterRepository.delete(projectID);
		ArrayList<String> objectIds = new ArrayList<String>();
		for (TaskMaster a:tempMaster.getTaskMaster())
			objectIds.add(a.getId());
		return objectIds;
	}

	@Override
	public void updateProject(ProjectCreationForm projectCreationForm) {
		ProjectMaster project = projectCreationForm.getProjectMaster();
		Query query = new Query();
		//		query.addCriteria(Criteria.where("id").is(project.getId()).and("projectName").is(project.getProjectName()));
//		query criteria is changed during the case of update as the update does not happen if the project name is changed (since it)
//		does not find a project with the new name. Thus the project name critera is removed.
		query.addCriteria(Criteria.where("id").is(project.getId()));		
		Update update = new Update();
		update.set("projectName", project.getProjectName());
		//		update.set("projectDuration", project.getProjectDuration());
		update.set("buMemberID", project.getBuMemberID());
		update.set("projectStartDate", project.getProjectStartDate());
		update.set("projectEndDate", project.getProjectEndDate());
		update.set("taskmaster", projectCreationForm.getTaskMasterList());
		update.set("currency", project.getCurrency());
		update.set("projectDescription", project.getProjectDescription());
		mongoOperations.updateFirst(query, update, ProjectMaster.class);// TODO Auto-generated method stub
	}

	@Override
	public ProjectMaster getProjectById(String projectID) {
		return projectMasterRepository.findOne(projectID);
	}

	@Override
	public List<ProjectMaster> findallProject() {
		return mongoOperations.findAll(ProjectMaster.class);
	}



}
