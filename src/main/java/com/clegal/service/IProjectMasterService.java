package com.clegal.service;

import java.util.ArrayList;
import java.util.List;

import com.clegal.model.ProjectMaster;
import com.clegal.wrapper.ProjectCreationForm;

/**Manipluations on the ProjectMaster table*/
public interface IProjectMasterService {

	void save(ProjectMaster project);

	ArrayList<String> deleteProject(String userId);

	void updateProject(ProjectCreationForm project);

	ProjectMaster getProjectById(String projectID);
	
	List<ProjectMaster> findallProjectByID(String buMemberID);
	
	List<ProjectMaster> findallProject();

}
