package com.clegal.service;

import java.util.ArrayList;

import org.bson.types.ObjectId;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
/**Interface for chat*/
public interface IChatMasterService {
	void updateChat(String id, ArrayList<ChatMessage> chatArray);

	void updateUnreadMessages(String id, ArrayList<ChatMessage> chatArray);
	
	void save(ChatMaster chatMaster);
	
	ChatMaster getChatById(String id);
}
