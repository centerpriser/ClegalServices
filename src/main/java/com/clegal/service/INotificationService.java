package com.clegal.service;

public interface INotificationService {

	void addInfoMessage(String msg);

	void addErrorMessage(String msg);

}
