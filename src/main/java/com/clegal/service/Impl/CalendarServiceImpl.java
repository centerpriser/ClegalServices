package com.clegal.service.Impl;


import com.clegal.model.OutlookEvent;
import com.clegal.model.UserAccount;
import com.clegal.repository.OutlookEventRepository;
import com.clegal.service.CalendarService;
import com.clegal.util.EventToOutlookEventMapper;
import com.outlook.dev.auth.TokenResponse;
import com.outlook.dev.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class CalendarServiceImpl implements CalendarService {

    @Autowired
    private OutlookEventRepository outlookEventRepository;

    @Override
    public void syncOutlookEvents(OutlookUser user, TokenResponse tokens) throws IOException {

//        Date now = new Date();
//        if (now.after(tokens.getExpirationTime())) {
//            // Token expired
//            // TODO: Use the refresh token to request a new token from the token endpoint
//            // For now, just complain
//            redirectAttributes.addFlashAttribute("error", "The access token has expired. Please logout and re-login.");
//            return "redirect:/index.html";
//        }

        System.out.println("EMAIL " + user.getMail());
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), user.getMail());


        String loggedInUser =  SecurityContextHolder.getContext().getAuthentication().getName();

        // Sort by start time in descending order
        String sort = "start/DateTime DESC";
        // Only return the properties we care about
        String properties = "organizer,subject,start,end";
        // Return at most 10 events


        PagedResult<Event> events = outlookService.getEvents(
                sort, properties, 100)
                .execute().body();

        if (events != null) {
            Collection<OutlookEvent> outlookEvents = new ArrayList<>();

            for (Event event : events.getValue()) {
                outlookEvents.add(EventToOutlookEventMapper.mapper(event, loggedInUser));

                System.out.println("Event Info " + event.getSubject());
            }

            outlookEventRepository.deleteOutLookEventsByUserId(loggedInUser);

            outlookEventRepository.save(outlookEvents);
        } else {
            System.out.println("NULL re jatho");
        }


    }


    @Override
    public Iterable<OutlookEvent> findAllByUsername(String loggedInUser) {
        return outlookEventRepository.findOutLookEventsByUserId(loggedInUser);
    }
}
