package com.clegal.service;

import com.clegal.model.Mailbox;
/**interface for manipulations on the TaskMaster table*/
public interface IMailboxService {
	
	void save(Mailbox mailbox);
}
