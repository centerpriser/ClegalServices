package com.clegal.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.clegal.model.Role;
import com.clegal.model.UserAccount;
import com.clegal.repository.RoleRepository;
import com.clegal.repository.UserAccountRepository;

/**CRUD operations on the UserAccount table*/
@Service
public class UserService implements IUserService, UserDetailsService {

	private final static Logger logger = LogManager.getLogger(UserService.class);

	
	@Autowired private UserAccountRepository UserAccountRepository;
	
	@Autowired private 	RoleRepository rolerepository;

	@Autowired private MongoOperations mongoOps;

	@Override
	public void save(UserAccount user) {
		logger.debug("inside userServiceIMpl");
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		//user.getRoles().forEach(arole -> user.addRole(arole));
		UserAccountRepository.save(user);
	}

	@Override
	public UserAccount findByUsername(String username) {
		return UserAccountRepository.findByUsername(username);
	}

	@Override
	public void deleteUser(String id) {
		Assert.notNull(id, "The given id must not be null!");
		UserAccount userAccount = UserAccountRepository.findOne(id);
		UserAccountRepository.delete(userAccount);
	}

	@Override
	public UserAccount getUserAccountById(String userId) {
		UserAccount userAccount = UserAccountRepository.findOne(userId);
		return userAccount;
	}

	@Override
	public void updateUser(UserAccount user) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(user.getId()).and("username").is(user.getUsername()));

		Update update = new Update();
		update.set("firstname", user.getFirstname());
		update.set("lastname", user.getLastname());
		update.set("enabled", user.isEnabled());
		update.set("status", user.getStatus());
		update.set("password", user.getPassword());
		update.set("resetToken", user.getResetToken());
		// Converting our set to Array
		Role[] arrayString = user.getRoles().toArray(new Role[user.getRoles().size()]);
		List<String> myroles = new ArrayList<String>();
		for (Role s : arrayString) {
			myroles.add(s.getId());
		}
		update.set("roles", myroles.toArray());
		mongoOps.updateFirst(query, update, UserAccount.class);
	}

	@Override
	public Role getRole(String role) { 
		System.out.println("Role Name:"+role);
		Role myrole = rolerepository.findOne(role);
		System.out.println(role);
		return myrole;
	}

	@Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAccount user = UserAccountRepository.findByUsername(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getId()));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }

	@Override
	public Optional<UserAccount> findByEmail(String email) {
		return UserAccountRepository.findByEmail(email);
	}
	
	public UserAccount findByConfirmationToken(String confirmationToken) {
		return UserAccountRepository.findByConfirmationToken(confirmationToken);
	}

	@Override
	public Optional<UserAccount> findUserByResetToken(String token) {
		return UserAccountRepository.findByResetToken(token);
	}
	
	/*@Override
	@Transactional(readOnly = true)
	public List<UserMaster> searchInUserMasterForAddress(String city, String domain){
		List<UserMaster> usermaster = userMasterRepository.findAll(city, domain);
		return usermaster;
	}*/


}
