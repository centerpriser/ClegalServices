package com.clegal.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IStorageService {
	
	public void store(MultipartFile file);
	public Resource loadFile(String filename);
	public void deleteFile(String fileName);
	public void deleteAll();
	public void init();
}
