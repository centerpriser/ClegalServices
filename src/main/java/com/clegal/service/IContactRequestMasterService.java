package com.clegal.service;

import java.util.ArrayList;

import com.clegal.model.ChatMessage;
import com.clegal.model.ContactRequestMaster;
/**Interface for chat*/
public interface IContactRequestMasterService {
	void updateChat(String id, ArrayList<ChatMessage> chatArray);

	void updateUnreadMessages(String id, ArrayList<ChatMessage> chatArray);
	
	void save(ContactRequestMaster chatMaster);
	
	ContactRequestMaster getChatById(String id);
}
