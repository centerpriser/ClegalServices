package com.clegal.service;

import java.util.Optional;

import com.clegal.model.Role;
import com.clegal.model.UserAccount;

public interface IUserService {
    void save(UserAccount userDetails);
    UserAccount findByUsername(String username);
    Optional<UserAccount> findByEmail(String email);    
    void deleteUser(String userId);
    void updateUser(UserAccount user);
    UserAccount getUserAccountById(String userId);
    Role getRole(String role);
    UserAccount findByConfirmationToken(String token);
	Optional<UserAccount> findUserByResetToken(String token);
}
