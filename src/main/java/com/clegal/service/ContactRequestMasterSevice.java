package com.clegal.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.clegal.model.ChatMessage;
import com.clegal.model.ContactRequestMaster;
import com.clegal.repository.ContactRequestMasterRepository;
/**Implementation of chat service*/
@Service
public class ContactRequestMasterSevice implements IContactRequestMasterService{
	
	@Autowired private MongoOperations mongoOperations;
	@Autowired private ContactRequestMasterRepository contactRequestMasterRepository;

	@Override
	public void updateChat(String id, ArrayList<ChatMessage> chatArray) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		Update update = new Update();
		update.set("unreadMessages", chatArray);
		mongoOperations.updateFirst(query, update, ContactRequestMaster.class);// TODO Auto-generated method stub
	}
	
	@Override
	public void updateUnreadMessages(String id, ArrayList<ChatMessage> chatArray) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		Update update = new Update();
		update.set("unreadMessages", chatArray);
		mongoOperations.updateFirst(query, update, ContactRequestMaster.class);// TODO Auto-generated method stub
	}

	@Override
	public void save(ContactRequestMaster chatMaster) {
		contactRequestMasterRepository.save(chatMaster);
		
	}
	
	@Override
	public ContactRequestMaster getChatById(String id) {
		return contactRequestMasterRepository.findOne(id);
	}	
}
