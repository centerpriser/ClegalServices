package com.clegal.service;

import java.io.FileReader;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.clegal.model.TaskMaster;
import com.clegal.model.UserMaster;
/**To be deprecated soon*/
@Component
@Profile({"dev", "default"})
public class DataInitializer {

	TaskMaster taskmaster = new TaskMaster();
	UserMaster usermaster = new UserMaster();

	@Autowired private MongoOperations operations;
	
	@Autowired private UserService userService;
	
    @Autowired private PasswordEncoder encoder; 
    
    @Autowired protected DbService dbService;
	
	@PostConstruct
	public void init() {
		
		//clear all collections, but leave indexes intact
		//dbService.cleanUp();
		
		//establish roles
		/*operations.insert(new Role("ROLE_USER"), "role");
		operations.insert(new Role("ROLE_ADMIN"), "role");
		operations.insert(new Role("ROLE_BU"), "role");
		
		UserInformation user = new UserInformation();
		user.setUsername("ravi");
		user.setFirstname("Ravi");
		user.setLastname("Kumar");
		user.setPassword("test");
		user.addRole(userService.getRole("ROLE_ADMIN"));
		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());	
		user.setEmail("ravi@ravi.com");
		//simulate account activation
		user.setEnabled(true);
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		userService.save(user);
		
		user = new UserInformation();
		user.setUsername("ankuj");
		user.setFirstname("Ankuj");
		user.setLastname("Arora");
		user.setEmail("ankuj@ankuj.com");
		user.setPassword("test");
		user.addRole(userService.getRole("ROLE_USER"));
		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());	
		user.setEnabled(true);
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		userService.save(user);*/
		
//		UserInformation user = new UserInformation();
//		user.setUsername("sylvain");
//		user.setFirstname("sylvain");
//		user.setLastname("flicoteaux");
//		user.setEmail("sylvain@centerpriser.com");
//		user.setPassword("test");
//		user.addRole(userService.getRole("ROLE_BU"));
//		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());	
//		user.setEnabled(true);
//		user.setAccountNonExpired(true);
//		user.setAccountNonLocked(true);
//		user.setCredentialsNonExpired(true);
//		userService.save(user);
		
		JSONParser parser = new JSONParser();
		try{
			
			Object obj = parser.parse(new FileReader("/Users/ankujarora1/git/ClegalServices/src/crawling/crawlBarreauGrenoble/BarreauGrenoblInfo.json"));

            JSONArray msg = (JSONArray) obj;
            Iterator<Object> iterator = msg.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
//	adding temporary tasks to test the visualization
	
}
