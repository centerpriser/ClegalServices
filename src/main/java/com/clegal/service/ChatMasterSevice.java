package com.clegal.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
import com.clegal.repository.ChatMasterRepository;
/**Implementation of chat service*/
@Service
public class ChatMasterSevice implements IChatMasterService{
	
	@Autowired private MongoOperations mongoOperations;
	@Autowired private ChatMasterRepository chatMasterRepository;

	@Override
	public void updateChat(String id, ArrayList<ChatMessage> chatArray) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		Update update = new Update();
		update.set("chatArray", chatArray);
		mongoOperations.updateFirst(query, update, ChatMaster.class);// TODO Auto-generated method stub
	}
	
	@Override
	public void updateUnreadMessages(String id, ArrayList<ChatMessage> chatArray) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		Update update = new Update();
		update.set("unreadMessages", chatArray);
		mongoOperations.updateFirst(query, update, ChatMaster.class);// TODO Auto-generated method stub
	}

	@Override
	public void save(ChatMaster chatMaster) {
		chatMasterRepository.save(chatMaster);
		
	}
	
	@Override
	public ChatMaster getChatById(String id) {
		return chatMasterRepository.findOne(id);
	}	
}
