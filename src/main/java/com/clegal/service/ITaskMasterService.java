package com.clegal.service;

import java.util.List;

import com.clegal.model.TaskMaster;
/**interface for manipulations on the TaskMaster table*/
public interface ITaskMasterService {
	
	void save(TaskMaster task);

	void deleteTask(String taskId);

	void updateTask(TaskMaster task);

	TaskMaster getTaskById(String taskID);
	
	Iterable<TaskMaster> findallTaskByID(String buMemberID);
	
	List<TaskMaster> findallTask();

}
