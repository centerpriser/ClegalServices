package com.clegal.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

/**Service for file management*/
@Service
public class StorageService implements IStorageService {
	
	Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	//String dirPath = env.getProperty("upload.files.directory");
	@Value("${upload.files.directory}") 
	private String uploadFileDirectory;
	
	private final Path rootLocation = Paths.get("D://Dev//temp");

	@Override
	public void store(MultipartFile file) {
		try {
			if(null != file)
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()),StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
        	throw new RuntimeException("FAIL!"+e.getMessage());
        }

	}  

	@Override
	public Resource loadFile(String filename) {
		 try {
	            Path file = rootLocation.resolve(filename);
	            Resource resource = new UrlResource(file.toUri());
	            if(resource.exists() || resource.isReadable()) {
	                return resource;
	            }else{
	            	throw new RuntimeException("FAIL!");
	            }
	        } catch (MalformedURLException e) {
	        	throw new RuntimeException("FAIL!");
	        }
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());

	}

	@Override
	public void init() {
		try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
	}

	@Override
	public void deleteFile(String fileName) {
		try {
            Path file = rootLocation.resolve(fileName);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
               try {
				Files.delete(file);
			} catch (IOException e) {
				throw new RuntimeException("FAIL, to Delete");
			}
            }else{
            	throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
        	throw new RuntimeException("FAIL!");
        }
		
	}

}
