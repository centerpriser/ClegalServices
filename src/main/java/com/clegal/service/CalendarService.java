package com.clegal.service;

import com.clegal.model.OutlookEvent;
import com.outlook.dev.auth.TokenResponse;
import com.outlook.dev.service.OutlookUser;

import java.io.IOException;

public interface CalendarService {
    void syncOutlookEvents(OutlookUser request, TokenResponse tokenResponse) throws IOException;

    Iterable<OutlookEvent> findAllByUsername(String loggedInUser);
}
