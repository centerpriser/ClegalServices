package com.clegal.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.clegal.model.TaskMaster;
import com.clegal.repository.TaskMasterRepository;

/**CRUD operations on the TaskMaster table*/
@Service
public class TaskMasterService implements ITaskMasterService {
	
	@Autowired private TaskMasterRepository taskMasterRepository;
	@Autowired private MongoOperations mongoOperations;

	@Override
	public void save(TaskMaster task) {
		taskMasterRepository.save(task);
	}

	@Override
	public void deleteTask(String taskId) {
		taskMasterRepository.delete(taskId);
	}

	@Override
	public void updateTask(TaskMaster task) {
		Query query = new Query();
		
//		query.addCriteria(Criteria.where("id").is(task.getId()).and("tname").is(task.gettName()));
//		tname criteria is removed as it does not update if the task name is changed
		query.addCriteria(Criteria.where("id").is(task.getId()));
		Update update = new Update();
		update.set("tName", task.gettName());
//		update.set("tDuration", task.gettDuration());
		update.set("taskStartDate", task.getTaskStartDate());
		update.set("taskEndDate", task.getTaskEndDate());
		update.set("uploadFileIDS", task.getUploadFileIDS());
		update.set("tDescription", task.gettDescription()); 
		update.set("taskPrice", task.getTaskPrice()); 
		mongoOperations.updateFirst(query, update,TaskMaster.class);// TODO Auto-generated method stub
	}

	@Override
	public TaskMaster getTaskById(String taskID) {
		return taskMasterRepository.findOne(taskID);
	}

	@Override
	public Iterable<TaskMaster> findallTaskByID(String buMemberID) {
		List<String> idList = new ArrayList<String>();
		idList.add(buMemberID);
		return taskMasterRepository.findAll(idList);
	}

	@Override
	public List<TaskMaster> findallTask() {
		return mongoOperations.findAll(TaskMaster.class);
	}

}
