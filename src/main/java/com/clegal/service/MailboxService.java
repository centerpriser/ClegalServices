package com.clegal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.clegal.model.Mailbox;
import com.clegal.repository.MailboxRepository;

/**CRUD operations on the TaskMaster table*/
@Service
public class MailboxService implements IMailboxService {
	
	@Autowired private MailboxRepository mailboxRepository;
	@Autowired private MongoOperations mongoOperations;

	@Override
	public void save(Mailbox mailbox) {
		mailboxRepository.save(mailbox);
	}
}
