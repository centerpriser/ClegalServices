package com.clegal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@EnableIntegration
@Configuration
// http://docs.spring.io/spring-boot/docs/current/reference/html/howto-security.html
// Switch off the Spring Boot security configuration

/**Security configuration parameters, deciding which URLs can be accessed by what stature of individuals*/
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private SimpleAuthenticationSuccessHandler successHandler;
    
    @Autowired
    PersistentTokenRepository repository;
    
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    // Bean to keep track of active users
    @Bean
    public ActiveUserStore activeUserStore(){
        return new ActiveUserStore();
    }
    
//    @Bean
//    public IntegrationFlow imapIdleFlow() {
//        return IntegrationFlows
//                .from(Mail.imapIdleAdapter("imap://user:pw@localhost:" + "993" + "/INBOX")
//                        .autoStartup(true)
//                        .searchTermStrategy(this::fromAndNotSeenTerm)
//                        .userFlag("testSIUserFlag")
//                        .javaMailProperties(p -> p.put("mail.debug", "false")
//                                .put("mail.imap.connectionpoolsize", "5"))
//                        .shouldReconnectAutomatically(false)
//                        .headerMapper(mailHeaderMapper()))
//                .channel(MessageChannels.queue("imapIdleChannel"))
//                .get();
//    }
//
//    @Bean
//    public HeaderMapper<MimeMessage> mailHeaderMapper() {
//        return new DefaultMailHeaderMapper();
//    }
//
//    private SearchTerm fromAndNotSeenTerm(Flags supportedFlags, Folder folder) {
//        try {
//            FromTerm fromTerm = new FromTerm(new InternetAddress("bar@baz"));
//            return new AndTerm(fromTerm, new FlagTerm(new Flags(Flags.Flag.SEEN), false));
//        }
//        catch (AddressException e) {
//            throw new RuntimeException(e);
//        }
//
//    }    
    @Autowired
    private LogoutSuccessHandler myLogoutSuccessHandler;
    
    // roles admin allow to access /admin/**
    // roles user allow to access /user/**
    // custom 403 access denied handler
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().authorizeRequests().antMatchers("/", "/home", "/*.html", "/search", "/autocomp", "/autocompDomain", "/signup", "/forgot", "/reset", "/confirm", "/locale", "/readMore", "/confirmationMessageNotification",
        		"/mail/sendAttachment", "/sendFileForVideoCall", "/termsConditions", "/cookiePolicy", "/ws/**", "/call", "/stomp", "/websocket/**", "/privacyPolicy", "/clientNotification", "/unregisteredUserDashboard", "/markMessagesAsRead", "/videoCall").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ADMIN").antMatchers("/user/**").hasAnyRole("USER").anyRequest()
                .authenticated().and().formLogin().successHandler(successHandler).loginPage("/login").permitAll().and()
                .rememberMe().rememberMeCookieName("javasampleapproach-remember-me").tokenValiditySeconds(24 * 60 * 60).tokenRepository(repository).and()
                .logout().logoutUrl("/logout").logoutSuccessHandler(myLogoutSuccessHandler).and().exceptionHandling()
                .accessDeniedPage("/errorpage");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    // Spring Boot configured this already.
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/css/bootstrap/**", "/css/fonts/**", "/icons/**",
                        "/fonts/**", "/js/**", "/img/**", "/images/**")
                .antMatchers("/static/eadmin/**", "/eadmin/css/**", "/eadmin/bootstrap/css/**",
                        "/eadmin/images/icons/css/**", "/eadmin/css/fonts/**", "/eadmin/bootstrap/js/**",
                        "/eadmin/scripts/**", "/eadmin/images/**");
    }
    
}
