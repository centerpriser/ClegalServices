package com.clegal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

import com.clegal.controllers.MyAppAuditor;

/**Auditor configuration file for security*/
@Configuration
@EnableMongoAuditing
public class Config {
	
	@Bean
    public AuditorAware<String> myAuditorProvider() {
        return new MyAppAuditor();
    }
}
