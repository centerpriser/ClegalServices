package com.clegal.wrapper;

import java.util.ArrayList;

import javax.validation.Valid;

import com.clegal.model.ProjectMaster;
import com.clegal.model.TaskMaster;

/**Wrapper for the Project Creation functionality*/
public class ProjectCreationForm{
	@Valid
	private ProjectMaster projectMaster;
	@Valid
	private ArrayList<TaskMaster> taskMasterList;
	
	public ProjectMaster getProjectMaster() {
		return projectMaster;
	}
	public void setProjectMaster(ProjectMaster projectMaster) {
		this.projectMaster = projectMaster;
	}
	public ArrayList<TaskMaster> getTaskMasterList() {
		return taskMasterList;
	}
	public void setTaskMasterList(ArrayList<TaskMaster> taskMasterList) {
		this.taskMasterList = taskMasterList;
	}
	
}