package com.clegal.wrapper;

import com.clegal.model.LawyerInformation;
import com.clegal.model.UserAccount;

/**Wrapper for the Lawyer personal page details*/
public class LawyerDetailForm{
	
	private UserAccount userAccount;
	private LawyerInformation lawyerInformation;
	public UserAccount getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	public LawyerInformation getLawyerInformation() {
		return lawyerInformation;
	}
	public void setLawyerInformation(LawyerInformation lawyerInformation) {
		this.lawyerInformation = lawyerInformation;
	}
}