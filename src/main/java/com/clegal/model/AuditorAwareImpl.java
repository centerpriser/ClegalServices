package com.clegal.model;

import org.springframework.data.domain.AuditorAware;
/**Returns the auditor of the application*/
class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        return "helplicitAdmin";
        // Can use Spring Security to return currently logged in user
        // return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()
    }
}