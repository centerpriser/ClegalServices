package com.clegal.model;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;
/**DB table that stores all the tasks specific to each project*/
@Document(collection = "taskMaster")
public class TaskMaster extends AbstractAuditable<String> {
	
	@Id
	private String id;
//	@DBRef(db="projectID")
	private String projectID;
	private Integer taskID;
	private String projectName;
	@NotNull
	@NotEmpty(message = "{NotNull.projectinformation.taskName}")	
	private String tName;
	private Integer tDuration;
	private Date taskStartDate;
	private Date taskEndDate;
	private String tStories;
	private String tStatus;
	private Integer tpriority;
	private String tDescription;
	private Double taskPrice;
	@Transient
	private ArrayList<MultipartFile> files;
	private ArrayList<String> uploadFileIDS;
	private ArrayList<String> downloadFileURIs;
	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}

	public Integer gettDuration() {
		return tDuration;
	}

	public void settDuration(Integer tDuration) {
		this.tDuration = tDuration;
	}

	public String gettStories() {
		return tStories;
	}

	public void settStories(String tStories) {
		this.tStories = tStories;
	}

	public String gettStatus() {
		return tStatus;
	}

	public void settStatus(String tStatus) {
		this.tStatus = tStatus;
	}

	public Integer getTpriority() {
		return tpriority;
	}

	public void setTpriority(Integer tpriority) {
		this.tpriority = tpriority;
	}

	public ArrayList<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<MultipartFile> files) {
		this.files = files;
	}

	public Integer getTaskID() {
		return taskID;
	}

	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}

	public Date getTaskStartDate() {
		return taskStartDate;
	}

	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	public Date getTaskEndDate() {
		return taskEndDate;
	}

	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	public ArrayList<String> getUploadFileIDS() {
		return uploadFileIDS;
	}

	public void setUploadFileIDS(ArrayList<String> uploadFileIDS) {
		this.uploadFileIDS = uploadFileIDS;
	}

	public ArrayList<String> getDownloadFileURIs() {
		return downloadFileURIs;
	}

	public void setDownloadFileURIs(ArrayList<String> downloadFileURIs) {
		this.downloadFileURIs = downloadFileURIs;
	}

	public String gettDescription() {
		return tDescription;
	}

	public void settDescription(String tDescription) {
		this.tDescription = tDescription;
	}

	public Double getTaskPrice() {
		return taskPrice;
	}

	public void setTaskPrice(Double taskPrice) {
		this.taskPrice = taskPrice;
	}

}
