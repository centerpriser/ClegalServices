package com.clegal.model;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
/**Table that stores all messages by a potential client to the lawyer they wish to message or get into a video call with.*/
@Document(collection = "contactRequestMaster")
public class ContactRequestMaster extends AbstractAuditable<String>  {

	@Id
	private String id; 
		
	private ArrayList<ChatMessage> chatArray;
	private ArrayList<ChatMessage> unreadMessages;	
	//	end user ID
	private String euID;
	//	business user ID
	private String buID;
	private String cookieID;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public ArrayList<ChatMessage> getChatArray() {
		return chatArray;
	}

	public void setChatArray(ArrayList<ChatMessage> chatArray) {
		this.chatArray = chatArray;
	}

	public ArrayList<ChatMessage> getUnreadMessages() {
		return unreadMessages;
	}

	public void setUnreadMessages(ArrayList<ChatMessage> unreadMessages) {
		this.unreadMessages = unreadMessages;
	}

	public String getEuID() {
		return euID;
	}

	public void setEuID(String euID) {
		this.euID = euID;
	}

	public String getBuID() {
		return buID;
	}

	public void setBuID(String buID) {
		this.buID = buID;
	}

	public String getcookieID() {
		return cookieID;
	}

	public void setcookieID(String cookieID) {
		this.cookieID = cookieID;
	}
}
