package com.clegal.model;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
/**Table that stores all the chat conversations. There is a difference between what the system
 * considers a notification message and what it considers a chat message. A notification is registered when an unverified user sends a
 * message asking for a meeting or a video conference. In this case, the request is not associated with a project, and shows up on the notification bar on the 
 * lawyers' dashboard. On the other hand, a chat message is one that is sent by a registered user who is already associated to a project by the lawyer. In this case
 * the chat messages are associated to specific projects and show up on the chat message bar on the lawyer dashboard.*/
@Document(collection = "chatMaster")
public class ChatMaster extends AbstractAuditable<String>  {

	@Id
	private String id; 
		
	private ArrayList<ChatMessage> chatArray;
	private ArrayList<ChatMessage> unreadMessages;	
	//	end user ID
	private String euID;
	//	business user ID
	private String buID;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public ArrayList<ChatMessage> getChatArray() {
		return chatArray;
	}

	public void setChatArray(ArrayList<ChatMessage> chatArray) {
		this.chatArray = chatArray;
	}

	public ArrayList<ChatMessage> getUnreadMessages() {
		return unreadMessages;
	}

	public void setUnreadMessages(ArrayList<ChatMessage> unreadMessages) {
		this.unreadMessages = unreadMessages;
	}

	public String getEuID() {
		return euID;
	}

	public void setEuID(String euID) {
		this.euID = euID;
	}

	public String getBuID() {
		return buID;
	}

	public void setBuID(String buID) {
		this.buID = buID;
	}
}
