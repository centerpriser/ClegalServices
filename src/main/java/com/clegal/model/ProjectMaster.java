package com.clegal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.clegal.util.AppContext;
/**DB table that contains all the project related information*/
@Document(collection = "projectMaster")
public class ProjectMaster extends AbstractAuditable<String>  {

	@Id
	private String id; 
	
	@NotNull
	@NotEmpty(message = "{NotNull.projectinformation.projectname}")
	private String projectName;
	
	private String buMemberID;
//	temporarily remove as it may not be needed
//	private Integer projectDuration;
//	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@NotNull(message = "{NotNull.projectinformation.projectStartDate}")
	private Date projectStartDate;
	@NotNull(message = "{NotNull.projectinformation.projectEndDate}")
	private Date projectEndDate;
	private String projectID;
	@NotNull
	@NotEmpty(message = "{NotNull.projectinformation.endUserID}")	
	private String endUserID;
	@DBRef
	private ArrayList<TaskMaster> taskmaster;
	@DBRef
	private ChatMaster chatMaster;
	private Currency currency;
	private String projectDescription;
	public static enum Currency {

	    EURO("currency.euro"),
	    DOLLAR("currency.dollar");

	    private final String displayName;

	    Currency(String displayName) {
	        this.displayName = displayName;
	    }

	    public String getDisplayName() {
	    	Locale locale = LocaleContextHolder.getLocale();
	    	System.out.println("locale### "+locale);
	    	System.out.println("AppContext.getApplicationContext().getMessage(displayName, null, locale) "+AppContext.getApplicationContext().getMessage(displayName, null, locale));
	    	return AppContext.getApplicationContext().getMessage(displayName, null, locale);
	    }
	}
  
	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuMemberID() {
		return buMemberID;
	}

	public void setBuMemberID(String buMemberID) {
		this.buMemberID = buMemberID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public Integer getProjectDuration() {
//		return projectDuration;
//	}
//
//	public void setProjectDuration(Integer projectDuration) {
//		this.projectDuration = projectDuration;
//	}

	public ArrayList<TaskMaster> getTaskMaster() {
		return taskmaster;
	}

	public void setTaskMaster(ArrayList<TaskMaster> taskmaster) {
		this.taskmaster = taskmaster;
	}
	
	public ChatMaster getchatMaster() {
		return chatMaster;
	}

	public void setchatMaster(ChatMaster chatMaster) {
		this.chatMaster = chatMaster;
	}

	public String getEndUserID() {
		return endUserID;
	}

	public void setEndUserID(String endUserID) {
		this.endUserID = endUserID;
	}

	public Date getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public Date getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

}
