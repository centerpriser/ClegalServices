package com.clegal.model;

import java.time.Instant;
import java.util.Date;

/**
 * Created by ankuj to characterize chat messages being exchanged between the lawyer and the client.
 */
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;
    private Instant timestamp;
    private String receiver;
    private String documentID;
    private String cookieID;
    
    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

    public ChatMessage(){}
    
    public ChatMessage(String sender, String receiver, String content) {
    	this.sender = sender;
    	this.receiver = receiver;
    	this.content = content;
	}

	public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date date) {
		Instant instant = date.toInstant();
		this.timestamp = instant;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getDocumentID() {
		return documentID;
	}

	public void setProjectID(String projectID) {
		this.documentID = projectID;
	}

	public String getcookieID() {
		return cookieID;
	}

	public void setcookieID(String cookieID) {
		this.cookieID = cookieID;
	}
}
