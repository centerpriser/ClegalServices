package com.clegal.model;
import java.util.ArrayList;

import javax.mail.Message;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Ankuj containing configurations for email storing.
 */
@Document(collection = "mailbox")
public class Mailbox extends AbstractAuditable<String>{
	@Id
	private String id; 
	private String subject;

	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
}