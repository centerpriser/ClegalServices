package com.clegal.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
/**All user related information, right from sign up to email confirmation, account activation, forgotten password functionality, as well as login*/
@Document(collection = "userAccount")
public class UserAccount extends AbstractAuditable<String> {

	@Id
	protected String id;
	//taking out the unique=true condition as multiple lawyers of the same firm may have the same email
//	@Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
	@NotEmpty(message = "{NotNull.userinformation.email}")
	protected String email;
	
	@Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
	@NotNull
	@NotEmpty(message = "{NotNull.userinformation.username}")
	private String username;
	@NotNull
	@Size(min = 6, message = "{passLength.userinformation.password}")
	private String password;
	private boolean accountNonExpired;
	private boolean accountNonLocked;
	private boolean credentialsNonExpired;
	private boolean tempPassword;
	private boolean enabled;
	private String address;
	private String status;
//	@NotEmpty(message = "{NotNull.userinformation.firstName}")
	private String firstname;
//	@NotEmpty(message = "{NotNull.userinformation.lastName}")	
	private String lastname;
	private Boolean isLawyer;
	private String confirmationToken;
	private String signinError;
	private String ipAddress;
	@DBRef
	private LawyerInformation lawyerInformation;
	// private Set<GrantedAuthority> grantedAuthorities;

	@DBRef
//	@Size(min = 1, message = "Please select a role.")
	private Set<Role> roles = new HashSet<Role>();
	private String resetToken;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void addRole(Role role) {
		this.roles.add(role);
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void removeRole(Role role) {
		// use iterator to avoid java.util.ConcurrentModificationException with
		// foreach
		for (Iterator<Role> iter = this.roles.iterator(); iter.hasNext();) {
			if (iter.next().equals(role))
				iter.remove();
		}
	}

	public String getRolesCSV() {
		StringBuilder sb = new StringBuilder();
		for (Iterator<Role> iter = this.roles.iterator(); iter.hasNext();) {
			sb.append(iter.next());
			if (iter.hasNext()) {
				sb.append(',');
			}
		}
		return sb.toString();
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof UserAccount)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		UserAccount rhs = (UserAccount) obj;
		return new EqualsBuilder().append(id, rhs.id).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(id).append(username).toHashCode();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Boolean getIsLawyer() {
		return isLawyer;
	}

	public void setIsLawyer(Boolean isLawyer) {
		this.isLawyer = isLawyer;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	public String getSigninError() {
		return signinError;
	}

	public void setSigninError(String signinError) {
		this.signinError = signinError;
	}

	public boolean isTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(boolean tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LawyerInformation getLawyerInformation() {
		return lawyerInformation;
	}

	public void setLawyerInformation(LawyerInformation lawyerInformation) {
		this.lawyerInformation = lawyerInformation;
	}

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}