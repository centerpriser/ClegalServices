package com.clegal.model;

public enum Gender {
    MALE,
    FEMALE
}