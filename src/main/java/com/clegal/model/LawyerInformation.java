 package com.clegal.model;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import com.clegal.util.AppContext;
/**DB table that stores all lawyer specific information*/
@Document
public class LawyerInformation extends UserAccount{

//	@Id
//	private String id; 
	private Gender gender;
	private UserType usertype;
	private String contactNumber;
	private String domainExpertise;
	private String qualification;
	private String yearofexperience;
	private String consultationcharges;
	private String practiveInCourt;
	private String location;
	private PaymentSchedule paymentSchedule;
	private String previousClients;
	private Rating rating;
	private String specialities;
	private String lawyerDescription;
	private Integer toqueNumber;
	private String siteName;
	private Map<String, String> workSchedule;
	private List<PaymentMode> paymentMode;
	private String pricePerHour;
	private MultipartFile photo;
	//	perhaps an enum of spoken languages might not be a good idea,
	//	as we should be capable of providing plurilingual support.
	//	Replace the enum with a list of strings for the moment.
	//	private List<SpokenLanguage> languages;  
	private List<String> languages;
	private String education;
	private LawyerType lawyerType;
	//enum explaining the type of the lawyer
	public static enum LawyerType {

	    ASSOCIATED("lawyertype.associated"),
	    INDEPENDENT("lawyertype.independent");

	    private final String displayName;

	    LawyerType(String label) {
	        this.displayName = label;
	    }

	    public String getDisplayName() {
	    	Locale locale = LocaleContextHolder.getLocale();
	    	return AppContext.getApplicationContext().getMessage(displayName, null, locale);
	    }
	}
	private LanguageSpoken languageSpoken;
	//enum explaining the type of the lawyer
	public static enum LanguageSpoken {

	    AFRIKANNS("lawyertype.associated"),
	    ALBANIAN("lawyertype.independent"),
	    ARABIC("lawyertype.associated"),
	    ARMENIAN("lawyertype.associated"),
	    BASQUE("lawyertype.associated"),
	    BENGALI("lawyertype.associated"),
	    BULGARIAN("lawyertype.associated"),
	    CATALAN("lawyertype.associated"),
	    CAMBODIAN("lawyertype.associated"),
	    CHINESE_MANDARIN("lawyertype.associated"),
	    CROATIAN("lawyertype.associated"),
	    CZECH("lawyertype.associated"),
	    DANISH("lawyertype.associated"),
	    DUTCH("lawyertype.associated"),
	    ENGLISH("lawyertype.associated"),
	    ESTONIAN("lawyertype.associated"),
	    FIJI("lawyertype.associated"),
	    FINNISH("lawyertype.associated"),
	    FRENCH("lawyertype.associated"),
	    GEORGIAN("lawyertype.associated"),
	    GERMAN("lawyertype.associated"),
	    GREEK("lawyertype.associated"),
	    GUJARATI("lawyertype.associated"),
	    HEBREW("lawyertype.associated"),
	    HINDI("lawyertype.associated"),
	    HUNGARIAN("lawyertype.associated"),
	    ICELANDIC("lawyertype.associated"),
	    INDONESIAN("lawyertype.associated"),
	    IRISH("lawyertype.associated"),
	    ITALIAN("lawyertype.associated"),
	    JAPANESE("lawyertype.associated"),
	    JAVANESE("lawyertype.associated"),
	    KOREAN("lawyertype.associated"),
	    LATIN("lawyertype.associated"),
	    LATVIAN("lawyertype.associated"),
	    LITHUANIAN("lawyertype.associated"),
	    MACEDONIAN("lawyertype.associated"),
	    MALAY("lawyertype.associated"),
	    MALAYALAM("lawyertype.associated"),
	    MALTESE("lawyertype.associated"),
	    MAORI("lawyertype.associated"),
	    MARATHI("lawyertype.associated"),
	    MONGOLIAN("lawyertype.associated"),
	    NEPALI("lawyertype.associated"),
	    NORWEGIAN("lawyertype.associated"),
	    PERSIAN("lawyertype.associated"),
	    POLISH("lawyertype.associated"),
	    PORTUGUESE("lawyertype.associated"),
	    PUNJABI("lawyertype.associated"),
	    QUECHUA("lawyertype.associated"),
	    ROMANIAN("lawyertype.associated"),
	    RUSSIAN("lawyertype.associated"),
	    SAMOAN("lawyertype.associated"),
	    SERBIAN("lawyertype.associated"),
	    SLOVAK("lawyertype.associated"),
	    SLOVENIAN("lawyertype.associated"),
	    SPANISH("lawyertype.associated"),
	    SWAHILI("lawyertype.associated"),
	    SWEDISH("lawyertype.associated"),
	    TAMIL("lawyertype.associated"),
	    TATAR("lawyertype.associated"),
	    TELEGU("lawyertype.associated"),
	    THAI("lawyertype.associated"),
	    TIBETIAN("lawyertype.associated"),
	    TONGA("lawyertype.associated"),
	    TURKISH("lawyertype.associated"),
	    UKRANIAN("lawyertype.associated"),
	    URDU("lawyertype.associated"),
	    VIETNAMESE("lawyertype.associated"),
	    WELSH("lawyertype.associated"),
	    XHOSA("lawyertype.associated");

	    private final String displayName;

	    LanguageSpoken(String label) {
	        this.displayName = label;
	    }

	    public String getDisplayName() {
	    	Locale locale = LocaleContextHolder.getLocale();
	    	return AppContext.getApplicationContext().getMessage(displayName, null, locale);
	    }
	}	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public UserType getUsertype() {
		return usertype;
	}

	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}


	public String getDomainExperties() {
		return domainExpertise;
	}

	public void setDomainExperties(String domainExperties) {
		this.domainExpertise = domainExperties;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getYearofexperience() {
		return yearofexperience;
	}

	public void setYearofexperience(String yearofexperience) {
		this.yearofexperience = yearofexperience;
	}

	public String getConsultationcharges() {
		return consultationcharges;
	}

	public void setConsultationcharges(String consultationcharges) {
		this.consultationcharges = consultationcharges;
	}

	public String getPractiveInCourt() {
		return practiveInCourt;
	}

	public void setPractiveInCourt(String practiveInCourt) {
		this.practiveInCourt = practiveInCourt;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public PaymentSchedule getPaymentSchedule() {
		return paymentSchedule;
	}

	public void setPaymentSchedule(PaymentSchedule paymentSchedule) {
		this.paymentSchedule = paymentSchedule;
	}

	public String getPreviousClients() {
		return previousClients;
	}

	public void setPreviousClients(String previousClients) {
		this.previousClients = previousClients;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getLawyerDescription() {
		return lawyerDescription;
	}

	public void setLawyerDescription(String lawyerDescription) {
		this.lawyerDescription = lawyerDescription;
	}

	public Integer getToqueNumber() {
		return toqueNumber;
	}

	public void setToqueNumber(Integer toqueNumber) {
		this.toqueNumber = toqueNumber;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Map<String, String> getWorkSchedule() {
		return workSchedule;
	}

	public void setWorkSchedule(Map<String, String> workSchedule) {
		this.workSchedule = workSchedule;
	}

	public List<PaymentMode> getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(List<PaymentMode> paymentMode) {
		this.paymentMode = paymentMode;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> spokenLanguage) {
		this.languages = spokenLanguage;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getSpecialities() {
		return specialities;
	}

	public void setSpecialities(String specialities) {
		this.specialities = specialities;
	}

	public String getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(String pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	public MultipartFile getPhoto() {
		return photo;
	}

	public void setPhoto(MultipartFile photo) {
		this.photo = photo;
	}

	public LawyerType getLawyerType() {
		return lawyerType;
	}

	public void setLawyerType(LawyerType lawyerType) {
		this.lawyerType = lawyerType;
	}
}