package com.clegal.model;

public enum PaymentSchedule {
	ONETIME,INSTALLMENTS, HOURLY_PRICE
}
