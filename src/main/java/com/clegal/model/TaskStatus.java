package com.clegal.model;

public enum TaskStatus {
	
	UPLOADED("UPLOADED"), SUBMITTED("SUBMITTED"), IGNORED("IGNORED"), REJECTED("REJECTED"), VALIDATED("VALIDATED"), PROCESSING("PROCESSING"), IMPORTED("IMPORTED"), ERROR("ERROR"), CANCELLED("CANCELLED"),FINISHED("FINISHED");

    private String _value = null;

    private TaskStatus(String value) {
        _value = value;
    }

    /**
     * Standard getter for value
     * 
     * @return the value
     */
    public String getValue() {
        return _value;
    }
	

}
