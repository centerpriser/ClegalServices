package com.clegal.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

public abstract class AbstractAuditable<U> {

	@CreatedBy
	@Order(Ordered.LOWEST_PRECEDENCE - 3)
	private String createdBy;

	@CreatedDate
	@Order(Ordered.LOWEST_PRECEDENCE - 2)
	private Instant createdDate;

	@LastModifiedBy
	@Order(Ordered.LOWEST_PRECEDENCE - 1)
	private String lastModifiedBy;

	@LastModifiedDate
	@Order(Ordered.LOWEST_PRECEDENCE)
	private Instant lastModifiedDate;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		 //LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
