package com.clegal.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**To be deprecated soon*/
@Document(collection = "userMaster")
public class UserMaster {

	@Id
	private String id; 
	private String userid;
	private String fname;
	private String lname;
	private String name;
	private String email;
	private String password;
	private Gender gender;
	private UserType usertype;
	private String contactNumber;
	private String domainExpertise;
	private String qualification;
	private String yearofexperience;
	private String consultationcharges;
	private String practiveInCourt;
	private String location;
	private PaymentSchedule paymentSchedule;
	private String previousClients;
	private Rating rating;
	private String address;
	private String specialities;
	private String lawyerDescription;
	private Integer toqueNumber;
	private String status;
	private String siteName;
	private Map<String, String> workSchedule;
	private List<PaymentMode> paymentMode;
	private String pricePerHour;
	
	//	perhaps an enum of spoken languages might not be a good idea,
	//	as we should be capable of providing plurilingual support.
	//	Replace the enum with a list of strings for the moment.
	//	private List<SpokenLanguage> languages;
	private List<String> languages;
	private String education;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.userid = name;
	}
	
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public UserType getUsertype() {
		return usertype;
	}

	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}


	public String getDomainExperties() {
		return domainExpertise;
	}

	public void setDomainExperties(String domainExperties) {
		this.domainExpertise = domainExperties;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getYearofexperience() {
		return yearofexperience;
	}

	public void setYearofexperience(String yearofexperience) {
		this.yearofexperience = yearofexperience;
	}

	public String getConsultationcharges() {
		return consultationcharges;
	}

	public void setConsultationcharges(String consultationcharges) {
		this.consultationcharges = consultationcharges;
	}

	public String getPractiveInCourt() {
		return practiveInCourt;
	}

	public void setPractiveInCourt(String practiveInCourt) {
		this.practiveInCourt = practiveInCourt;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public PaymentSchedule getPaymentSchedule() {
		return paymentSchedule;
	}

	public void setPaymentSchedule(PaymentSchedule paymentSchedule) {
		this.paymentSchedule = paymentSchedule;
	}

	public String getPreviousClients() {
		return previousClients;
	}

	public void setPreviousClients(String previousClients) {
		this.previousClients = previousClients;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getLawyerDescription() {
		return lawyerDescription;
	}

	public void setLawyerDescription(String lawyerDescription) {
		this.lawyerDescription = lawyerDescription;
	}

	public Integer getToqueNumber() {
		return toqueNumber;
	}

	public void setToqueNumber(Integer toqueNumber) {
		this.toqueNumber = toqueNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Map<String, String> getWorkSchedule() {
		return workSchedule;
	}

	public void setWorkSchedule(Map<String, String> workSchedule) {
		this.workSchedule = workSchedule;
	}

	public List<PaymentMode> getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(List<PaymentMode> paymentMode) {
		this.paymentMode = paymentMode;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> spokenLanguage) {
		this.languages = spokenLanguage;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getSpecialities() {
		return specialities;
	}

	public void setSpecialities(String specialities) {
		this.specialities = specialities;
	}

	public String getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(String pricePerHour) {
		this.pricePerHour = pricePerHour;
	}
}
