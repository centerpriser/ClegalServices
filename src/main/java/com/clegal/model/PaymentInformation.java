 package com.clegal.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**DB table that stores all lawyer specific information*/
@Document
public class PaymentInformation extends UserAccount{

//	@Id
//	private String id; 
	private String cardNumber;
	private String expiryDate;
	private String cvvNumber;
	private String paymentGateway;
	
	//	perhaps an enum of spoken languages might not be a good idea,
	//	as we should be capable of providing plurilingual support.
	//	Replace the enum with a list of strings for the moment.
	//	private List<SpokenLanguage> languages;  
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCvvNumber() {
		return cvvNumber;
	}

	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

}