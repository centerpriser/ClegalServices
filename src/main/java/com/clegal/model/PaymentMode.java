package com.clegal.model;

public enum PaymentMode {
	CHEQUE, ONLINETRANSFER, CASH;
}
