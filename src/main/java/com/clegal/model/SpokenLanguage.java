package com.clegal.model;

public enum SpokenLanguage {
	ENGLISH, FRENCH, SPANISH;
}
