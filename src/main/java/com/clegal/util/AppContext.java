package com.clegal.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
@Component
public class AppContext implements ApplicationContextAware{ 

    private static ApplicationContext ctx; 

    /**
     * Injected from the class "ApplicationContextProvider" which is automatically
     * loaded during Spring-Initialization. This class is used to provide for the context
     * which is then used to inject the specific messages.properties file
     */ 
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) { 
        ctx = applicationContext; 
    } 

    /**
     * Get access to the Spring ApplicationContext from everywhere in your Application
     *
     * @return
     */ 

    public static ApplicationContext getApplicationContext() { 
        return ctx; 
    } 
}