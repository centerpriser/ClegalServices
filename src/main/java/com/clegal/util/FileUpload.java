package com.clegal.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.gridfs.GridFSDBFile;

public abstract class FileUpload{
	
	//file upload
	protected final GridFsTemplate gridFsTemplate;
    
	@Value("${upload.files.directory}")
	private String uploadFileDirectory;     
	
    @Autowired
    public FileUpload(GridFsTemplate gridFsTemplate){
    	this.gridFsTemplate = gridFsTemplate;
    }
    
    //save file
    private void saveUploadedFiles(List<MultipartFile> files) throws IOException {

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }

            byte[] bytes = file.getBytes();
            Path path = Paths.get(uploadFileDirectory + file.getOriginalFilename());
            Files.write(path, bytes);

        }

    }
    
	  protected Optional<GridFSDBFile> maybeLoadFile(String name) {
		    GridFSDBFile file = gridFsTemplate.findOne(getFilenameQuery(name));
		    //save the file back to the disk
		    if(file!=null){
					try {
						String fileName = file.getFilename();
						
						file.writeTo(uploadFileDirectory + fileName);
						
						System.out.println("Text File Name: " + fileName);
						
					} catch (IOException e) {
						e.printStackTrace();
					}		    	
		    }
		    return Optional.ofNullable(file);
		  }

		  protected static Query getFilenameQuery(String name) {
		    return Query.query(GridFsCriteria.whereFilename().is(name));
		  }    
    
    
}