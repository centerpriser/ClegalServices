package com.clegal.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class ClegalDateTimeUtil {

	/*public static String clegalDateTimeFormatter(Instant instant,Locale yourLocale) {
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withLocale(yourLocale)
				.withZone(ZoneId.systemDefault());
		return formatter.format(instant);
	}

	public static void main(String[] args) {
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		    String timestamp = "2016-02-16 11:00:02";
		    TemporalAccessor temporalAccessor = formatter.parse(timestamp);
		    LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
		    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
		    Instant result = Instant.from(zonedDateTime);
		    System.out.println(result);
	}*/
	
	public static void main(String... args) {
        Instant instant = Instant.parse("2017-09-14T12:02:12.535Z");
        System.out.println(instant);
        
        
        ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZonedDateTime.now().getZone());
        System.out.println("ZDT:>"+zdt);
        
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        
        System.out.println("dateTime:>"+dateTime);

        //until(i, OffsetDateTime.now());
        //until(i, ZonedDateTime.now());
        //System.out.println(dateCheck(instant));
    }

    private static void until(Instant i, Temporal temporal) {
        System.out.printf("--Temporal: %s > %s%n",
                temporal.getClass().getSimpleName(), temporal);

        for (ChronoUnit unit : ChronoUnit.values()) {
            try {
                long l = i.until(temporal, unit);
                System.out.printf("%10s > %s%n", unit, l);
            } catch (Exception e) {
                //ignore unsupported units
            }
        }
    }
    
    /*private static String dateCheck(Instant instant)
    {
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return formatter;
    }*/
}
