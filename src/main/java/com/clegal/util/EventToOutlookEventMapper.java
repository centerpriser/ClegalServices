package com.clegal.util;

import com.clegal.model.OutlookEvent;
import com.outlook.dev.service.Event;

public class EventToOutlookEventMapper {

    public static OutlookEvent mapper(Event event, String userId) {
        OutlookEvent outlookEvent = new OutlookEvent();

        outlookEvent.setEnd(event.getEnd().getDateTime());
        outlookEvent.setStart(event.getStart().getDateTime());
        outlookEvent.setOrganizerEmail(event.getOrganizer().getEmailAddress().getAddress());
        outlookEvent.setSubject(event.getSubject());
        outlookEvent.setUserId(userId);

        return outlookEvent;
    }
}
