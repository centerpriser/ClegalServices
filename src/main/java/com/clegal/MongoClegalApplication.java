package com.clegal;



import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.kurento.client.KurentoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.clegal.service.IMailboxService;
import com.clegal.service.StorageService;
import com.clegal.videochat.CallHandler;
import com.clegal.videochat.UserRegistry;

/**Starting point of the application, contains the main method*/
@PropertySource("classpath:application.properties")
//@ImportResource("/mail/integration.xml")
@SpringBootApplication(exclude={org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration.class} )
@EnableMongoRepositories
@EnableWebSocket
public class MongoClegalApplication extends WebMvcConfigurerAdapter implements WebSocketConfigurer{
//public class MongoClegalApplication extends WebMvcConfigurerAdapter{

	@Autowired
	IMailboxService mailboxService;
	@Resource
	StorageService storageService;
	@Value("${upload.files.directory}")
	private String uploadFileDirectory;
	private static final Logger logger = LoggerFactory.getLogger(MongoClegalApplication.class);	

	public static void main(String[] args) throws MessagingException, IOException {
//        // 1. Setup properties for the mail session.
//        Properties props = new Properties();
//        props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.pop3.socketFactory.fallback", "false");
//        props.put("mail.pop3.socketFactory.port", "995");
//        props.put("mail.pop3.port", "995");
//        props.put("mail.pop3.host", "imap.gmail.com");
//        props.put("mail.pop3.user", "ankujarora");
//        props.put("mail.store.protocol", "imaps");
//
//        // 2. Creates a javax.mail.Authenticator object.
//        Authenticator auth = new Authenticator() {
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication("ankujarora", "keepout21!");
//            }
//        };
//
//        // 3. Creating mail session.
//        Session session = Session.getDefaultInstance(props, auth);
//
//        // 4. Get the POP3 store provider and connect to the store.
//        Store store = session.getStore("imaps");
//        store.connect("pop.gmail.com", "ankujarora", "keepout21!");
//
//        
//        //in the meanwhile, get the names of all the folders
//        Folder[] folder = store.getDefaultFolder().list(); // get the array folders in server
//        int i=0;
//        for( i=0;i<folder.length;i++)
//        System.out.println("Press "+i+" to read "+folder[i].getName()+" folder");
//        
//        // 5. Get folder and open the INBOX folder in the store.
//        Folder inbox = store.getFolder("mavefreaks");
//        inbox.open(Folder.READ_ONLY);
//
//        // 6. Retrieve the messages from the folder.
//        Message[] messages = inbox.getMessages();
//        for (Message message : messages) {
//        	mailboxService.save(message);
//        	Flags flag = message.getFlags();
//        	String[] flag1 = flag.getUserFlags();
//        	for (String flag2:flag1){
//        		System.out.println("flag2 "+flag2);
//        	}
//        
//        	if (message.getFlags().contains(Flag.SEEN)){
//        		System.out.println("message "+message.getSubject());
//        	}
//
////            message.writeTo(System.out);
//        }
//
//        // 7. Close folder and close store.
//        inbox.close(false);
//        store.close();
		SpringApplication.run(MongoClegalApplication.class, args);
		logger.debug("--Application Started--");
	}

	public void run(String... args) throws Exception {
		System.out.println("Directory ::: Upload >>"+uploadFileDirectory);
		//storageService.deleteAll();
		storageService.init();
	}

//	configurations added for forwarding http requests to https
	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
	  TomcatEmbeddedServletContainerFactory tomcat =
	      new TomcatEmbeddedServletContainerFactory() {

	        @Override
	        protected void postProcessContext(Context context) {
	          SecurityConstraint securityConstraint = new SecurityConstraint();
	          securityConstraint.setUserConstraint("CONFIDENTIAL");
	          SecurityCollection collection = new SecurityCollection();
	          collection.addPattern("/*");
	          securityConstraint.addCollection(collection);
	          context.addConstraint(securityConstraint);
	        }
	      };
	  tomcat.addAdditionalTomcatConnectors(createHttpConnector());
	  return tomcat;
	}

	@Value("${server.port.http}")
	private int serverPortHttp;

	@Value("${server.port}")
	private int serverPortHttps;

	private Connector createHttpConnector() {
	  Connector connector =
	      new Connector("org.apache.coyote.http11.Http11NioProtocol");
	  connector.setScheme("http");
	  connector.setSecure(false);
	  connector.setPort(serverPortHttp);
	  connector.setRedirectPort(serverPortHttps);
	  return connector;
	}

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("/eadmin/login");
    }

//    @Autowired
//    private MessageSource messageSource;

//    @Bean
//    public MessageSource messageSource() {
//        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.setBasenames("classpath:messages");
//        messageSource.setUseCodeAsDefaultMessage(true);
//        messageSource.setDefaultEncoding("UTF-8");
//        messageSource.setCacheSeconds(5);
//        return messageSource;
//    }
//
	@Override
    public Validator getValidator() {
        LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
        factory.setValidationMessageSource(messageSource());
        return factory;
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
//        return new CookieLocaleResolver();
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
    
    //Beans specific to video call via Kurento
    @Bean
    public CallHandler callHandler() {
      return new CallHandler();
    }

    @Bean
    public UserRegistry registry() {
      return new UserRegistry();
    }

    @Bean
    public KurentoClient kurentoClient() {
//      return KurentoClient.create("wss://helplicit.com:8433/kurento");
      return KurentoClient.create("wss://34.254.185.103:8433/kurento");      
//    	SslContextFactory sec = new SslContextFactory(true);
//    	sec.setValidateCerts(false);
//    	JsonRpcClientWebSocket rpcClient = new JsonRpcClientWebSocket("wss://34.254.185.103:8433/kurento", sec);
//    	KurentoClient kuretoClient = KurentoClient.createFromJsonRpcClient(rpcClient);    	
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    	System.out.println("callhandler "+callHandler().toString());
    	System.out.println("registry "+registry.toString());
      registry.addHandler(callHandler(), "/call").setAllowedOrigins("*");
    }    

}
