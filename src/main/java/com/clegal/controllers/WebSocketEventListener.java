package com.clegal.controllers;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.clegal.model.ChatMessage;

/**
 * Created by ankujarora to handle websocket based requests coming via the end to end user chat.
 */
@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleSubscribeEvent(SessionSubscribeEvent event) {
    	if(event.getUser() != null){
        	logger.info("in the subscription method with username "+event.getUser().getName());
        	messagingTemplate.convertAndSendToUser(event.getUser().getName(), "/queue/notification", "GREETINGS");
    	}
    	else{
    		logger.info("in the subscription method ");
    	}
    }
    
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        Set<String> chatMembersList = (Set<String>) headerAccessor.getSessionAttributes().get("chatMembersList");
        chatMembersList.remove((String) headerAccessor.getSessionAttributes().get("username"));
        System.out.println("chatMembersList "+chatMembersList);
        if(username != null) {
            logger.info("User Disconnected : " + username);

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setType(ChatMessage.MessageType.LEAVE);
            chatMessage.setSender(username);

            messagingTemplate.convertAndSendToUser(username, "/queue/notification", chatMessage);
        }
    }
}
