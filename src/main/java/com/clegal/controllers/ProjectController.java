package com.clegal.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
import com.clegal.model.ProjectMaster;
import com.clegal.model.TaskMaster;
import com.clegal.model.UserAccount;
import com.clegal.repository.ChatMasterRepository;
import com.clegal.repository.UserAccountRepository;
import com.clegal.service.IProjectMasterService;
import com.clegal.service.ITaskMasterService;
import com.clegal.util.FileUpload;
import com.clegal.wrapper.ProjectCreationForm;
import com.mongodb.gridfs.GridFSDBFile;

/**Controller for all project manipulation tasks*/

@SessionAttributes("projectCreationForm")
@RestController
public class ProjectController extends FileUpload{

	@Autowired
	IProjectMasterService projectMasterService;
	
	@Autowired
	ITaskMasterService taskMasterService;

	@Autowired
	UserAccountRepository userInformationRepository;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true));
	}
	
	private ArrayList<TaskMaster> taskMaster;
	
	private ArrayList<TaskMaster> taskMasterList = new ArrayList<TaskMaster>();
	
	private ProjectCreationForm projectCreationForm;
	
	@Autowired
	private ChatMasterRepository chatMasterRepository;
	
    
	@Value("${upload.files.directory}")
	private String uploadFileDirectory;    
    
    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);	
    
    private static final String VALIDATION_REGEX = "^[0-9]+(,[0-9]+)*$";
    
    @Autowired
    public ProjectController(GridFsTemplate gridFsTemplate){
    	super(gridFsTemplate);
    }
	
	@RequestMapping(value = "/saveProject", method = RequestMethod.POST)
	public ModelAndView saveProject(@RequestParam("action") String action, @Valid @ModelAttribute ProjectCreationForm projectCreationForm, BindingResult result,
			ModelAndView model) {
		logger.debug("in the saveProject method");
		System.out.println("tempProjectMaster.getCurrency() "+projectCreationForm.getProjectMaster().getCurrency());
		System.out.println("value of action is "+ action);
	    System.out.println("the start date is "+ projectCreationForm.getProjectMaster().getProjectStartDate());
	    System.out.println("the end date is "+ projectCreationForm.getProjectMaster().getProjectEndDate());
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		UserAccount userInformation = userInformationRepository.findByUsername(loggedInUser);
		if(userInformation.getIsLawyer())model.addObject("isBusinessUser", userInformation.getIsLawyer());
		if(action.equals("addTask")){
			//taskMasterList.addAll(projectCreationForm.getTaskMasterList());
			taskMasterList = projectCreationForm.getTaskMasterList();
	    	taskMasterList.add(new TaskMaster());
	    	System.out.println("size of list here is"+projectCreationForm.getProjectMaster().getProjectName());
	    	projectCreationForm.setTaskMasterList(taskMasterList);
	    	System.out.println("size of list is"+taskMasterList.size());
	    	model.setViewName("eadmin/manageProject");
	    	model.addObject("projectCreationForm", projectCreationForm);
	    	return model;
		}
		
		System.out.println("The size of the form is"+projectCreationForm.getTaskMasterList().size());
		for (TaskMaster taskList:projectCreationForm.getTaskMasterList()){
			System.out.println("task is: "+taskList.gettName());
			System.out.println("currency is: "+taskList.getTaskPrice());
			ArrayList<MultipartFile> files = taskList.getFiles();
			ArrayList<String> fileUploadIDS = null;
			if(taskList.getUploadFileIDS()==null)
				fileUploadIDS = new ArrayList<String>();
			else
				fileUploadIDS = taskList.getUploadFileIDS();
			for (MultipartFile file:files){
				String name = file.getOriginalFilename();
				System.out.println("name of the file is "+ file.getOriginalFilename());
				if(!name.isEmpty()){
				    try {
					      Optional<GridFSDBFile> existing = super.maybeLoadFile(name);
					      if (existing.isPresent()) {
					        super.gridFsTemplate.delete(super.getFilenameQuery(name));
					        System.out.println("existing.get().getId() "+existing.get().getId());
//					        fileUploadIDS.remove(existing.get().getId());
					      }
					      String fileStorageID = gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).getId().toString();
					      System.out.println("fileStorageID "+fileStorageID);
					      fileUploadIDS.add(fileStorageID);
					      taskList.setUploadFileIDS(fileUploadIDS);
					      System.out.println("name "+name);
					      System.out.println("id "+fileStorageID);
				          // Get the file and save it somewhere
				          byte[] bytes = file.getBytes();
				          Path path = Paths.get(uploadFileDirectory + file.getOriginalFilename());
				          Files.write(path, bytes);
				          String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				                  .path("/downloadFile/")
				                  .path(name)
				                  .toUriString();
				          System.out.println("fileDownloadUri "+fileDownloadUri);
				  		// read file from MongoDB
				  		GridFSDBFile imageFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(fileStorageID)));
				  		System.out.println("found file "+imageFile.getFilename());
				    } catch (IOException e) {
					      System.out.println("file is not uploaded");
				    }									
				}

			}
		}
		if (result.hasErrors()) {
			System.out.println("errors: "+ result.getAllErrors());
			model.setViewName("eadmin/manageProject");
			model.addObject("error", "Please correct the form errors.");
			
		} else {
			if (projectCreationForm.getProjectMaster().getId() == null) {
				ProjectMaster tempProjectMaster = projectCreationForm.getProjectMaster();
				for (TaskMaster task:projectCreationForm.getTaskMasterList()){
					System.out.println("task added: "+task.gettName());
					taskMasterService.save(task);
				}
				//add a dummy message in the chatMaster table so that the table gets created
				ChatMaster chatMaster = new ChatMaster();
				chatMaster.setChatArray(new ArrayList<ChatMessage>());
				chatMaster.setBuID(loggedInUser);
				chatMasterRepository.save(chatMaster);
				//add the chat message list to the existing tempProjectMaster
				tempProjectMaster.setchatMaster(chatMaster);
				tempProjectMaster.setTaskMaster(projectCreationForm.getTaskMasterList());
				projectMasterService.save(tempProjectMaster);
				model.addObject("projectMasterList", projectMasterService.findallProjectByID(loggedInUser));
			} else {
				projectCreationForm.getProjectMaster().setTaskMaster(projectCreationForm.getTaskMasterList());				
				//update the TaskMaster table at first
				for(TaskMaster tm:projectCreationForm.getProjectMaster().getTaskMaster()){
					if(tm.getId()!=null){taskMasterService.updateTask(tm);}
					else taskMasterService.save(tm);
				}
				projectMasterService.updateProject(projectCreationForm);
				//figure out if logged in user is a business user or not
				model.addObject("projectMasterList", projectMasterService.findallProjectByID(loggedInUser));
			}
			model.addObject("message", "Successfully Updated!");
			model.addObject("userName", loggedInUser);
			model.setViewName("eadmin/viewProject");
		}
		return model;
	}
	

	/*Download the file that appears in the form of a hyperlink on the frontend screen shared between client and lawyer*/	  
    @GetMapping("/downloadFile")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName) throws IOException {
        // Load file as Resource
    	// Resource resource = fileStorageService.loadFileAsResource(fileName);
    	GridFSDBFile resource = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(fileName)));
        Optional<GridFSDBFile> existing = super.maybeLoadFile(fileName);
	      if (existing.isPresent()) {
	    	  resource = existing.get();
	      }
        // Try to determine file's content type
        String contentType = null;
        contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        Path path = Paths.get(uploadFileDirectory+resource.getFilename());
        ByteArrayResource res = new ByteArrayResource(Files.readAllBytes(path));
        return ResponseEntity
        		.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(res);
    }
		    
	@GetMapping(value = "/eadmin/deleteProject/{id}")
	public ModelAndView delteProject(@PathVariable("id") String id,ModelAndView model) {
		ArrayList<String> objectIDs = projectMasterService.deleteProject(id);
		for (String tm:objectIDs)
		taskMasterService.deleteTask(tm);
		model.addObject("message", "Successfully Done!");
		model.addObject("projectMasterList", projectMasterService.findallProject());
		model.setViewName("eadmin/viewProject");
		return model;
	}
	
	
	@GetMapping(value = "/eadmin/viewProject")
	public ModelAndView viewProject(ModelAndView model) {
		model.addObject("projectMasterList", projectMasterService.findallProject());
		model.setViewName("eadmin/viewProject");
		return model;
	}

	@GetMapping("/eadmin/manageProject/{id}")
	public ModelAndView manageProject(@PathVariable("id") String id, ModelAndView modelAndView) throws ParseException {
		logger.debug("in the manageProject method");
		ProjectMaster projectMaster = projectMasterService.getProjectById(id);
		ArrayList<TaskMaster> taskMasterList = new ArrayList<>();
		for (TaskMaster tm:projectMaster.getTaskMaster())
			taskMasterList.add(taskMasterService.getTaskById(tm.getId()));
		for (TaskMaster taskList:taskMasterList){
			ArrayList<String> downloadFileURIList = new ArrayList<String>();
			if(taskList.getUploadFileIDS()!=null){
				for (String fileID:taskList.getUploadFileIDS()){
					System.out.println("taskList "+taskList.gettName());
					System.out.println("fileID "+fileID);
			  		GridFSDBFile uploadedFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(fileID)));
					String name = uploadedFile.getFilename();
					//just use the filename instead of the entire file path as the donwload link on the html page will be restricted to just the file name
			        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			                  .path("/downloadFile/")
			                  .path(name)
			                  .toUriString();
			        downloadFileURIList.add(name);
				}
			}
			taskList.setDownloadFileURIs(downloadFileURIList);
		}
		modelAndView.addObject("projectMaster", projectMaster);
		modelAndView.addObject("projectMasterList", projectMasterService.findallProject());
		ProjectCreationForm projectCreationForm = new ProjectCreationForm();
		projectCreationForm.setProjectMaster(projectMaster);
		projectCreationForm.setTaskMasterList((ArrayList<TaskMaster>)taskMasterList);
		modelAndView.addObject("projectCreationForm", projectCreationForm);
		modelAndView.setViewName("eadmin/manageProject");
		return modelAndView;
	}

	@RequestMapping(value = "/manageProjectLandingPage", method = RequestMethod.GET)
	public ModelAndView manageProjectLandingPage(HttpSession session) {
		logger.debug("in the manageProjectLandingPage method");
		//find name of currently logged in user
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		//figure out if logged in user is a business user or not
		UserAccount userAccount = userInformationRepository.findByUsername(loggedInUser);
		taskMasterList = new ArrayList<TaskMaster>();
		projectCreationForm = new ProjectCreationForm();
		projectCreationForm.setProjectMaster(new ProjectMaster());
		projectCreationForm.setTaskMasterList(taskMasterList);
		ModelAndView model = new ModelAndView();
		model.addObject("projectCreationForm", projectCreationForm);
		model.addObject("projectMasterList", projectMasterService.findallProjectByID(loggedInUser));
		model.addObject("userName", loggedInUser);
		ArrayList<ChatMessage> unreadNotifications = chatMasterRepository.findUnreadNotifications(loggedInUser);
		System.out.println("unreadNotifications "+ unreadNotifications);
		model.addObject("notifications", chatMasterRepository.findUnreadNotifications(loggedInUser));
		//query to find all the unread chat messages, and send them to the client side in the form of notifications
		if(userAccount.getIsLawyer())model.addObject("isBusinessUser", userAccount.getIsLawyer());
		model.setViewName("eadmin/viewProject");
		session.setAttribute("projectCreationForm", projectCreationForm);
		return model;
	}
	
	
	@RequestMapping(value = "/createProject", method = RequestMethod.GET)
	public ModelAndView createProject() {
		//find name of currently logged in user
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		taskMasterList = new ArrayList<TaskMaster>();
		projectCreationForm = new ProjectCreationForm();
		projectCreationForm.setProjectMaster(new ProjectMaster());
		projectCreationForm.setTaskMasterList(taskMasterList);
		ModelAndView model = new ModelAndView(); 
		model.addObject("projectCreationForm", projectCreationForm);
		model.addObject("userName", loggedInUser);
		model.setViewName("eadmin/manageProject");
		return model;
	}	
	
//	@RequestMapping(value = "/manageProject", method = RequestMethod.GET)
//	public ModelAndView manageProject() {
//		ProjectMaster projectMaster = new ProjectMaster();
//		ArrayList<TaskMaster> taskMaster = new ArrayList<TaskMaster>();
//		ProjectCreationForm projectCreationForm = new ProjectCreationForm();
//		taskMaster.add(new TaskMaster());
//		projectCreationForm.setTaskMasterList(taskMaster);
//		ModelAndView model = new ModelAndView();
//		model.addObject("projectMaster", projectMaster);
//		model.addObject("projectCreationForm", projectCreationForm);
//		model.addObject("projectMasterList", projectMasterService.findallProject());
//		model.setViewName("eadmin/manageProject");
//		return model;
//	}

	@RequestMapping(value = "/manageProject", method = RequestMethod.POST)
	public ModelAndView manageProject(@RequestParam("taskName") String taskName, @RequestParam("taskDuration") Integer taskDuration) {
		System.out.println("in the manage method");
		ProjectMaster projectMaster = new ProjectMaster();
		ProjectCreationForm projectCreationForm = new ProjectCreationForm();
		taskMaster.add(new TaskMaster());
		projectCreationForm.setTaskMasterList(taskMaster);
		ModelAndView model = new ModelAndView();
		model.addObject("projectMaster", projectMaster);
		model.addObject("projectCreationForm", projectCreationForm);
		model.addObject("projectMasterList", projectMasterService.findallProject());
		model.setViewName("eadmin/manageProject");
		return model;
	}
	
//    @GetMapping("/addTask")
//    public ModelAndView addTask(
//    		HttpSession session)
//    {
//    	taskMasterList.add(new TaskMaster());
//    	ProjectCreationForm projectCreationForm = (ProjectCreationForm)session.getAttribute("projectCreationForm");
//    	System.out.println("size of list here is"+projectCreationForm.getProjectMaster().getProjectName());
//    	projectCreationForm.setTaskMasterList(taskMasterList);
//    	System.out.println("size of list is"+taskMasterList.size());
//    	ModelAndView model = new ModelAndView();
//    	model.setViewName("eadmin/manageProject");
//    	model.addObject("projectCreationForm", projectCreationForm);
//    	return model;
//    }

	
    
    //save file
    private void saveUploadedFiles(List<MultipartFile> files) throws IOException {

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }

            byte[] bytes = file.getBytes();
            Path path = Paths.get(uploadFileDirectory + file.getOriginalFilename());
            Files.write(path, bytes);

        }

    }
	@RequestMapping(value = "/reloadManageProject")
	public String reloadManageProject(@ModelAttribute("taskName") String taskName, @ModelAttribute("taskDuration") Integer taskDuration, @ModelAttribute("projectCreationForm") ProjectCreationForm projectCreationForm) {
		System.out.println("taskname: "+taskName+"taskDuration: "+taskDuration);
		ProjectMaster projectMaster = new ProjectMaster();
		TaskMaster taskMasterTemp = new TaskMaster();
		taskMasterTemp.settName(taskName);
		taskMasterTemp.settDuration(taskDuration);
		taskMaster.add(taskMasterTemp);
		projectCreationForm.setTaskMasterList(taskMaster);
		for(TaskMaster task:taskMaster){
			System.out.println("element is"+task.gettName());
		}
		System.out.println("size of list in form is: "+projectCreationForm.getTaskMasterList().size());
		ModelAndView model = new ModelAndView();
//		model.addObject("projectMaster", projectMaster);
//		model.addObject("projectCreationForm", projectCreationForm);
//		model.addObject("projectMasterList", projectMasterService.findallProject());
//		model.addObject("mailStatus", "{\"msg\":\"success\"}");
//		model.setViewName("eadmin/manageProject");
//		return model;
		return "redirect:/eadmin/manageProject";
	}
    
    
	/*@RequestMapping(value = "/eadmin/projectManage", method = RequestMethod.GET)
	public ModelAndView adminManageProject() {
		ProjectMaster projectMaster = new ProjectMaster();
		ModelAndView model = new ModelAndView();
		model.addObject("projectMaster", projectMaster);
		model.addObject("projectMasterList", projectMasterService.findallProject());
		model.setViewName("eadmin/adminManageProject");
		return model;
	}*/
	
	
}
