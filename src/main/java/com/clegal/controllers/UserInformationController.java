package com.clegal.controllers;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.clegal.model.EmailServiceImpl;
import com.clegal.model.MailObject;
import com.clegal.model.UserAccount;
import com.clegal.model.UserInformation;
import com.clegal.model.UserMaster;
import com.clegal.repository.RoleRepository;
import com.clegal.repository.UserAccountRepository;
import com.clegal.repository.UserAccountStatus;
import com.clegal.repository.UsermasterMongoRepository;
import com.clegal.repository.UsermasterSearchRepository;
import com.clegal.service.INotificationService;
import com.clegal.service.IUserService;
import com.clegal.wrapper.Pager;
import com.clegal.wrapper.SuggestionWrapper;
import com.google.maps.errors.ApiException;

/**All login, signup related endpoints are treated in this method*/
@Controller
public class UserInformationController {

//	private final static Logger logger = LogManager.getLogger(UserInformationController.class);
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE_SIZE = 10;
	private static final int INITIAL_PAGE = 0;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	UsermasterMongoRepository userMasterRepository;

	@Autowired
	UserAccountRepository userAccountRepository;

	@Autowired
	RoleRepository rolerepository;

	@Autowired
	IUserService userservices;
	
	@Autowired
	EmailServiceImpl emailService;
	
	@Autowired
	private INotificationService notifyService;
		
	@Autowired
	UsermasterSearchRepository usermasterSearchRepository;
	
	@Autowired UsermasterMongoRepository userMasterMongoRepo;

	@Value("#{'${data.cityOfFrance}'.split(',')}")
	private List<String> cityOfFrance;
	
	@Value("#{'${data.domainList}'.split(',')}")
	private List<String> dataDomainList;
	
	/**
	 * Master User Management
	 **/

	@RequestMapping("/")
	public ModelAndView index(ModelAndView model, HttpServletResponse response, @CookieValue(value="helplicit", required=false) String traCookie) {
//		ServletRequestAttributes attr = (ServletRequestAttributes) 
//			    RequestContextHolder.currentRequestAttributes();
//			HttpSession session= attr.getRequest().getSession(true); // true == allow create
//		UserAccount userAccount = userAccountRepository.findUserByIpAddress(request.getRemoteAddr());
//		if(userAccount!=null){
//			model.addObject("ipAddress", request.getRemoteAddr());
//		}
//	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    if(traCookie!=null){
	    	model.addObject("traCookie", traCookie);
	    }
//	    if(fooCookie==null){
//			Cookie cookie = new Cookie("helplicit", Long.toString(timestamp.getTime()));
//			cookie.setMaxAge(10 * 365 * 24 * 60 * 60); //set cookie for 10 years
//			response.addCookie(cookie); //add cookie to response
//	    }
//	    else{
//	    	System.out.println("foocookie## "+fooCookie);
//	    }
		model.setViewName("index");
		return model;
	}

	@RequestMapping(value = "/autocomp", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public SuggestionWrapper autoCompLocation(@RequestParam(value="location") String location, ModelAndView model) {
		ArrayList<String> suggestions = new ArrayList<>();
	    for (String city : cityOfFrance) {
	      if (city.toLowerCase().contains(location.toLowerCase())) {
	        suggestions.add(city);
	      }
	    }
	    // truncate the list to the first n, max 20 elements
	    int n = suggestions.size() > 20 ? 20 : suggestions.size();
	    List<String> sulb = new ArrayList<>(suggestions.subList(0, n));

	    SuggestionWrapper sw = new SuggestionWrapper();
	    sw.setSuggestions(sulb);
	    return sw;
	}


	@RequestMapping(value = "/autocompDomain", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public SuggestionWrapper autoCompDomain(@RequestParam(value="domain") String location, ModelAndView model) {
		ArrayList<String> suggestions = new ArrayList<>();
	    for (String city : dataDomainList) {
	      if (city.toLowerCase().contains(location.toLowerCase())) {
	        suggestions.add(city);
	      }
	    }
	    // truncate the list to the first n, max 20 elements
	    int n = suggestions.size() > 20 ? 20 : suggestions.size();
	    List<String> sulb = new ArrayList<>(suggestions.subList(0, n));

	    SuggestionWrapper sw = new SuggestionWrapper();
	    sw.setSuggestions(sulb);
	    return sw;
	}
	/*@RequestMapping("/home")
	public String login(Model model) {
		model.addAttribute("userMasterList", userMasterRepository.findAll());
		return "home";
	}*/

	@RequestMapping(value = "eadmin/addInUserAccount", method = RequestMethod.POST)
	public String addInUserAccount(@Valid @ModelAttribute UserInformation userInformation, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("userAccountList", userAccountRepository.findAll());
			model.addAttribute("usersRoleList", rolerepository.findAll());
			model.addAttribute("error", "Please correct the form error.");
			return "/eadmin/userAccountOperations";
		} else {
			userservices.save(userInformation);
			model.addAttribute("message", "Successfully saved!");
			return "redirect:userAccountOperations";
		}

	}
//	@RequestMapping(value = {"/search"})
////	public String search(@RequestParam(value="searchLawyer") String searchLawyer, Pageable pageable, Model model) {
//	public ModelAndView search(@RequestParam(value="searchLawyer") String searchLawyer, @RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page, Pageable pageable, ModelAndView model) {
//		System.out.println("the searchLawyer string is"+searchLawyer);
////		List<UserMaster> usermaster=userMasterRepository.searchLawyerFromUserMaster(searchLawyer);
////		List<UserMaster> usermaster=(List<UserMaster>) userMasterRepository.findAll();
////		PageWrapper<UserMaster> usermaster = new PageWrapper<UserMaster>(userMasterRepository.searchLawyerFromUserMaster(searchLawyer, pageable), "/searchResult");
//		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
//		pageable=new PageRequest(evalPage, 10);
//		Page<UserMaster> usermaster = userMasterMongoRepo.searchLawyerFromUserMaster(searchLawyer, pageable);
////		test with findall
////		Page<UserMaster> usermaster = userMasterRepository.findAll(pageable);
//		System.out.println("the number of pages are "+usermaster.getTotalPages());
//		System.out.println("the pageSize string is"+pageSize);
//		System.out.println("the page string is"+page);		
////		model.addAttribute("page", usermaster);
//		System.out.println("result is"+usermaster.getTotalPages());
//		List<UserMaster> usercontent=usermaster.getContent();
////		ModelAndView model = new ModelAndView("searchResult");
//		model.setViewName("searchResult");
//		model.addObject("persons", usermaster);
//		model.addObject("pageSizes", PAGE_SIZES);
//		Pager pager = new Pager(usermaster.getTotalPages(), usermaster.getNumber(), BUTTONS_TO_SHOW);
//		model.addObject("pager", pager);
//		model.addObject("searchLawyer", searchLawyer);
//		model.addObject("selectedPageSize", 10);
//		System.out.println("result of the first page is "+usercontent.get(0).getName());
////		return "searchResult";
//		model.addObject("mailObject", new MailObject());
//		return model;
//	}
	
	@RequestMapping(value = {"/search"})
	public ModelAndView search(@RequestParam(value="franceCity") String franceCity, @RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page, Pageable pageable, ModelAndView model, @CookieValue(value="ankuj", required=false) String fooCookie) {
		System.out.println("fooCookie "+fooCookie);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		pageable=new PageRequest(evalPage, 10);
		Page<UserAccount> usermaster = userAccountRepository.findUserMasterByRegexpAddress(franceCity.trim(), pageable);
		model.addObject("persons", usermaster);
		model.addObject("pageSizes", PAGE_SIZES);
		Pager pager = new Pager(usermaster.getTotalPages(), usermaster.getNumber(), BUTTONS_TO_SHOW);
		model.addObject("pager", pager);
		model.addObject("franceCity", franceCity);
		model.addObject("selectedPageSize", 10);
		model.addObject("mailObject", new MailObject());
		model.addObject("dataDominList", dataDomainList);
		model.setViewName("searchResult");
		return model;
	}
	
	@RequestMapping(value = {"/lawyerDetails/{id}"})
	public ModelAndView searchByPersonID(@PathVariable("id") String id, ModelAndView model) {
		System.out.println("the searchLawyer By ID is:>"+id);
		UserMaster usrMaster = userMasterRepository.findOne(id);
		model.addObject("laywerDetails",usrMaster);
		model.setViewName("searchResult::personTable");
		return model;
	}
	
	
	
	@RequestMapping("/home")
	public String login(Model model) {
		model.addAttribute("userMasterList", userMasterMongoRepo.findAll());
		return "home";
	}
	
	@RequestMapping(value = "/eadmin/editFromUserAccount/{id}", method = RequestMethod.GET)
	public ModelAndView editFromUserAccount(@PathVariable("id") String id) {
		UserAccount userInformation = userservices.getUserAccountById(id);
		ModelAndView modelView = new ModelAndView("userAccountManage");
		modelView.setViewName("/eadmin/userAccountManage");
		modelView.addObject("userInformation", userInformation);
		modelView.addObject("usersRoleList", rolerepository.findAll());
		return modelView;
	}

	@RequestMapping(value = "/eadmin/updateUserAccount", method = RequestMethod.POST)
	public ModelAndView updateUserAccount(@Valid @ModelAttribute UserInformation userInformation, BindingResult result,
			ModelAndView model) {
		if (result.hasErrors()) {
			model.setViewName("/eadmin/userAccountManage");
			model.addObject("userAccountList", userAccountRepository.findAll());
			model.addObject("usersRoleList", rolerepository.findAll());
			model.addObject("error", "Please correct the form error.");
		} else {
			model = new ModelAndView("userAccountOperations");
			model.addObject("userAccountList", userAccountRepository.findAll());
			model.addObject("usersRoleList", rolerepository.findAll());
			userservices.updateUser(userInformation);
			model.setViewName("/eadmin/userAccountOperations");
			model.addObject("message", "Successfully Updated!");
		}
		return model;
	}

	@RequestMapping(value = "eadmin/deleteFromUserAccount", method = RequestMethod.GET)
	public String deleteFromUserAccount(Model model, @RequestParam("id") String id) {
		userservices.deleteUser(id);
		return "redirect:/eadmin/userAccountOperations";
	}

	@RequestMapping(value = "/eadmin/userAccountOperations", method = RequestMethod.GET)
	public String userAccountOperations(Model model) {
		UserInformation userInformation = new UserInformation(); 
		model.addAttribute("userInformation", userInformation);
		model.addAttribute("userAccountList", userAccountRepository.findAll());
		model.addAttribute("usersRoleList", rolerepository.findAll());
		return "eadmin/userAccountOperations";
	}

	@RequestMapping(value = "/userAccountManage", method = RequestMethod.GET)
	public String userAccountManage(Model model) {
		return "/userAccountManage";
	}

	

	/**
	 * Add/Create Task
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws ApiException 
	 */

	/*@RequestMapping(value = "/listTask", method = { RequestMethod.GET, RequestMethod.POST })
	public String listTask(Model model) {
		model.addAttribute("taskMasterList", taskMasterRepository.findAll());
		return "createTask";
	}

	@RequestMapping(value = "/createTask", method = RequestMethod.POST)
	public String createTask(Model model, @ModelAttribute TaskMaster taskMaster) {
		taskMasterRepository.save(taskMaster);
		model.addAttribute("taskMasterList", taskMasterRepository.findAll());
		return "createTask";
	}*/

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String eadminLogin(Model model, String error, String logout) throws ApiException, InterruptedException, IOException {
		model.addAttribute("userAccount", new UserAccount());
		model.addAttribute("signinError", new String("false"));
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");
		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");
		return "eadmin/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String eadminLoginPOST(Model model, String error, String logout) throws ApiException, InterruptedException, IOException {
		model.addAttribute("userAccount", new UserAccount());
		model.addAttribute("signinError", new String("false"));
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");
		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");
		return "eadmin/login";
	}
//	//process registration form
//	// Process form input data
//	@RequestMapping(value = "/signup", method = RequestMethod.POST)
//	public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid @ModelAttribute UserInformation user, BindingResult bindingResult, HttpServletRequest request) {
//			
//		System.out.println("user.getEmail() "+user.getIsLawyer());
//		// Lookup user in database by e-mail
//		UserInformation userExists = userservices.findByEmail(user.getEmail());
//		
//		System.out.println(userExists);
//		
//		if (userExists != null) {
//			modelAndView.addObject("error", "Please correct the form errors");
//			modelAndView.setViewName("/eadmin/login");
//			bindingResult.reject("email");
//		}
//			
//		if(user.getIsLawyer()){
//			user.addRole(userservices.getRole("ROLE_BU"));
//		}else user.addRole(userservices.getRole("ROLE_USER"));
//		
//		if (bindingResult.hasErrors()) {
//			System.out.println("get all errors "+ bindingResult.getAllErrors());
//			System.out.println("in the error loop");
//			modelAndView.addObject("signupError", "Please correct the form errors");
//			modelAndView.addObject("user", user);
//			modelAndView.setViewName("/eadmin/login");		
//		} else { // new user so we create user and send confirmation e-mail
//					
//			
//			// Disable user until they click on confirmation link in email
//		    user.setEnabled(false);
//		      
//		    // Generate random 36-character string token for confirmation link
//		    user.setConfirmationToken(UUID.randomUUID().toString());
//		        
//			user.setStatus(UserAccountStatus.STATUS_PENDING.name());	
//			user.setEnabled(false);
//			user.setAccountNonExpired(true);
//			user.setAccountNonLocked(false);
//			user.setCredentialsNonExpired(true);
//		    userservices.save(user);
//				
//			String appUrl = request.getScheme() + "://" + request.getServerName();
//			MailObject mailObject = new MailObject();
//			mailObject.setTo(user.getEmail());
//			mailObject.setSubject("Registration Confirmation");
//			mailObject.setText("To confirm your e-mail address, please click the link below:\n"
//					+ appUrl + "/confirm?token=" + user.getConfirmationToken());
//			//mailObject.setFrom("noreply@domain.com");
//			
//			emailService.sendSimpleMessage(mailObject.getTo(), mailObject.getSubject(), mailObject.getText());
//			
//			modelAndView.addObject("confirmationMessage", "A confirmation e-mail has been sent to " + user.getEmail());
//			modelAndView.setViewName("index");
//		}
//			
//		return modelAndView;
//	}
	
	
	//process registration form
	// Process form input data
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String processRegistrationForm(ModelAndView modelAndView, @Valid @ModelAttribute UserAccount userAccount, BindingResult bindingResult, HttpServletRequest request,
			ModelMap model, RedirectAttributes redirectAttributes) {
			
		// Lookup user in database by e-mail
		Optional<UserAccount> userExists = userAccountRepository.findByEmail(userAccount.getEmail());
		if (userExists.isPresent()) {
			System.out.println("user exists");
//			modelAndView.addObject("error", "Please correct the form errors");
//			modelAndView.setViewName("/eadmin/login");
//			bindingResult.rejectValue("email", messageSource.getMessage("user.exists", null, LocaleContextHolder.getLocale()));
			FieldError fieldError = new FieldError("userAccount", "email", messageSource.getMessage("user.exists", null, LocaleContextHolder.getLocale()));
			bindingResult.addError(fieldError);
			userAccount.setSigninError("true");
//			return "eadmin/login";
		}
		if(userAccount.getIsLawyer()){
			userAccount.addRole(userservices.getRole("ROLE_BU"));
		}else userAccount.addRole(userservices.getRole("ROLE_USER"));
		if (bindingResult.hasErrors()) {
			System.out.println("get all errors "+ bindingResult.getAllErrors());
			System.out.println("in the error loop");
//			modelAndView.addObject("signupError", "Please correct the form errors");
//			modelAndView.addObject("user", user);
//			modelAndView.setViewName("/eadmin/login");
//	        redirectAttributes.addFlashAttribute("user", user);
//	        model.addAttribute("user", user);
			userAccount.setSigninError("true");
//	          return "eadmin/login";
		} else { // new user so we create user and send confirmation e-mail
					
			
			// Disable user until they click on confirmation link in email
		    userAccount.setEnabled(false);
		      
		    // Generate random 36-character string token for confirmation link
		    userAccount.setConfirmationToken(UUID.randomUUID().toString());
		        
			userAccount.setStatus(UserAccountStatus.STATUS_PENDING.name());	
			userAccount.setEnabled(false);
			userAccount.setAccountNonExpired(true);
			userAccount.setAccountNonLocked(false);
			userAccount.setTempPassword(false);
			userAccount.setCredentialsNonExpired(true);
		    userservices.save(userAccount);
				
			String appUrl = request.getScheme() + "://" + request.getServerName();
			MailObject mailObject = new MailObject();
			mailObject.setTo(userAccount.getEmail());
			mailObject.setSubject("Registration Confirmation");
			mailObject.setText("To confirm your e-mail address, please click the link below:\n"
					+ appUrl + "/confirm?token=" + userAccount.getConfirmationToken());
			//mailObject.setFrom("noreply@domain.com");
			
			emailService.sendSimpleMessage(mailObject.getTo(), mailObject.getSubject(), mailObject.getText());
			return "redirect:/confirmationMessageNotification";
			
//			modelAndView.addObject("confirmationMessageNotification", "A confirmation e-mail has been sent to " + userAccount.getEmail());
//			modelAndView.setViewName("eadmin/login");
		}
	    return "eadmin/login";
	}

	//this method is used to redirect the user to the login screen with a confirmation message of a confirmation mail being sent.
	@RequestMapping("/confirmationMessageNotification")
	public ModelAndView confirmationMessageNotification(ModelAndView modelAndView) {
		System.out.println("A confirmation e-mail has been sent");
		UserAccount userAccount = new UserAccount();
		//userAccount.setSigninError("true");
		modelAndView.addObject("userAccount", userAccount);
		System.out.println("A confirmation e-mail has been sent");
		modelAndView.addObject("confirmationMessageNotification", "A confirmation e-mail has been sent to " + userAccount.getEmail());
		modelAndView.setViewName("eadmin/login");
		return modelAndView;
	}	
	
	// Process confirmation link
	@RequestMapping(value="/confirm", method = RequestMethod.GET)
	public ModelAndView showConfirmationPage(ModelAndView modelAndView, @RequestParam("token") String token, BindingResult bindingResult) {
			
		System.out.println("in the confirm method");
		UserAccount user = userservices.findByConfirmationToken(token);
			
		if (user == null) { // No token found in DB
			System.out.println("token not found");
			modelAndView.addObject("invalidToken", "Oops!  This is an invalid confirmation link.");
			UserAccount userAccount  = new UserAccount();
			modelAndView.addObject("userAccount", userAccount);
			FieldError fieldError = new FieldError("userAccount", "email", messageSource.getMessage("invalid.confirmation.token", null, LocaleContextHolder.getLocale()));
			bindingResult.addError(fieldError);
			userAccount.setSigninError("true");
			modelAndView.addObject("tokenError", "The token is invalid!");
			modelAndView.setViewName("eadmin/login");
			return modelAndView;
		} else { // Token found
			user.setEnabled(true);
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setResetToken(null);
			user.setStatus(UserAccountStatus.STATUS_APPROVED.name());	
			user.setCredentialsNonExpired(true);
		    userservices.updateUser(user);
			modelAndView.addObject("confirmationToken", user.getConfirmationToken());
			
		}
		modelAndView.addObject("loggedInUser", SecurityContextHolder.getContext().getAuthentication().getName());
		modelAndView.setViewName("index");
		return modelAndView;		
	}
	
	
	
	
//	// Process confirmation link
//	@RequestMapping(value="/confirm", method = RequestMethod.POST)
//	public ModelAndView processConfirmationForm(ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map requestParams, RedirectAttributes redir) {
//				
//		modelAndView.setViewName("confirm");
//		
//		Zxcvbn passwordCheck = new Zxcvbn();
//		
//		Strength strength = passwordCheck.measure(requestParams.get("password"));
//		
//		if (strength.getScore() < 3) {
//			bindingResult.reject("password");
//			
//			redir.addFlashAttribute("errorMessage", "Your password is too weak.  Choose a stronger one.");
//
//			modelAndView.setViewName("redirect:confirm?token=" + requestParams.get("token"));
//			System.out.println(requestParams.get("token"));
//			return modelAndView;
//		}
//	
//		// Find the user associated with the reset token
//		User user = userService.findByConfirmationToken(requestParams.get("token"));
//
//		// Set new password
//		user.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
//
//		// Set user to enabled
//		user.setEnabled(true);
//		
//		// Save user
//		userService.saveUser(user);
//		
//		modelAndView.addObject("successMessage", "Your password has been set!");
//		return modelAndView;		
//	}
//	
//}

	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView admin() {
		ModelAndView model = new ModelAndView();
		model.setViewName("eadmin/index");
		return model;
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView user() {
		ModelAndView model = new ModelAndView();
		model.setViewName("/index");
		return model;
	}
	
	// Display forgotPassword page
	@GetMapping(value = "/forgot")
	public ModelAndView displayForgotPasswordPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("/eadmin/forgotPassword");
		return model;
    }
    
    // Process form submission from forgotPassword page
	@RequestMapping(value = "/forgot", method = RequestMethod.POST)
	public ModelAndView processForgotPasswordForm(ModelAndView modelAndView, @RequestParam("email") String userEmail, HttpServletRequest request) {

		// Lookup user in database by e-mail
		Optional<UserAccount> optional = userservices.findByEmail(userEmail);
		MailObject mailObject = null;
		
		if (!optional.isPresent()) {
			modelAndView.addObject("errorMessage", "We didn't find an account for that e-mail address.");
		} else {
			mailObject = new MailObject();
			// Generate random 36-character string token for reset password 
			UserAccount user = optional.get();
			user.setResetToken(UUID.randomUUID().toString());

			// Save token to database
			userservices.save(user);

			String appUrl = request.getScheme() + "://" + request.getServerName();
			
			// Email message
			mailObject.setTo(user.getEmail());
			mailObject.setSubject("Password Reset Request");
			mailObject.setText("To reset your password, click the link below:\n" + appUrl
					+ "/reset?token=" + user.getResetToken());
			
			emailService.sendSimpleMessage(user.getEmail(),mailObject.getSubject(), mailObject.getText());
			// Add success message to view
			modelAndView.addObject("successMessage", "A password reset link has been sent to " + userEmail);
		}

		modelAndView.setViewName("eadmin/forgotPassword");
		return modelAndView;

	}

	// Display form to reset password
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public ModelAndView displayResetPasswordPage(ModelAndView modelAndView, @RequestParam("token") String token) {
		
		Optional<UserAccount> user = userservices.findUserByResetToken(token);

		if (user.isPresent()) { // Token found in DB
			modelAndView.addObject("token", token);
		} else { // Token not found in DB
			modelAndView.addObject("errorMessage", "Oops!  This is an invalid password reset link.");
		}

		modelAndView.setViewName("eadmin/resetPassword");
		return modelAndView;
	}

	// Process reset password form
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public ModelAndView setNewPassword(ModelAndView modelAndView, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {
		
        System.out.println("requestParams.get(token)*** "+requestParams.get("token"));
		// Find the user associated with the reset token
		Optional<UserAccount> user = userservices.findUserByResetToken(requestParams.get("token"));

		// This should always be non-null but we check just in case
		if (user.isPresent()) {
			
			UserAccount resetUser = user.get(); 
            System.out.println("resetUser*** "+resetUser.getUsername());
			if(!(requestParams.get("password").equals(requestParams.get("confirmPassword")))){
				modelAndView.addObject("errorMessage", "Oops!  The entered passwords do not match.");
				modelAndView.setViewName("eadmin/resetPassword");
				return modelAndView;
			}
			// Set new password    
            resetUser.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
            
			// Set the reset token to null so it cannot be used again
			resetUser.setResetToken(null);

			// Save user
			userservices.updateUser(resetUser);

			// In order to set a model attribute on a redirect, we must use
			// RedirectAttributes
			modelAndView.addObject("userAccount", new UserAccount());
			modelAndView.addObject("successMessage", "You have successfully reset your password. You may now login.");
			modelAndView.setViewName("eadmin/login");
			return modelAndView;
			
		} else {
			modelAndView.addObject("errorMessage", "Oops!  This is an invalid password reset link.");
			modelAndView.setViewName("eadmin/resetPassword");	
		}
		
		return modelAndView;
   }
   
    // Going to reset page without a token redirects to login page
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ModelAndView handleMissingParams(MissingServletRequestParameterException ex) {
		return new ModelAndView("redirect:login");
	}
	
	//onclick of button for Kurento video call
	@GetMapping(value = "/videoCall/{client}")
	public ModelAndView kurentoVideoCall(@PathVariable("client") String client) {
//	@GetMapping(value = "/videoCall")
//	public ModelAndView kurentoVideoCall() {	
		 ModelAndView model = new ModelAndView();
		 String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		 UserAccount userAccount = userservices.findByUsername(loggedInUser);
		 model.addObject("username", loggedInUser);
		 model.addObject("isLawyer", userAccount.getIsLawyer());
		 model.addObject("client", client);
		 model.setViewName("/kurentoVideoCall");
		return model;
	}
	
	//onclick of button for Kurento video call
	@GetMapping(value = "/videoCall")
	public ModelAndView videoCallClientAccept(@RequestParam("name") String name, @RequestParam("id") String id, @RequestParam("from") String from) {
		 System.out.println("name$$$ "+name);
		 System.out.println("id$$$ "+id);
		 System.out.println("from$$$ "+from);
		 ModelAndView model = new ModelAndView();
		 String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		 model.addObject("username", loggedInUser);
		 model.addObject("from", from);
		 model.addObject("userAcceptCall", true);
		 model.setViewName("/kurentoVideoCall");
		return model;
	}	
}