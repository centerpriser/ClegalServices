package com.clegal.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.clegal.config.ActiveUserStore;
import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
import com.clegal.model.EmailServiceImpl;
import com.clegal.model.MailObject;
import com.clegal.model.UserAccount;
import com.clegal.repository.ChatMasterRepository;
import com.clegal.repository.UserAccountRepository;
import com.clegal.service.IProjectMasterService;
import com.clegal.util.FileUpload;
import com.mongodb.gridfs.GridFSDBFile;
/**Controller which acts as the landing for email requests and sends out the mails via the configured SMTP server*/
@Controller
@RequestMapping("/mail")
public class MailController extends FileUpload{

    public MailController(GridFsTemplate gridFsTemplate) {
		super(gridFsTemplate);
		// TODO Auto-generated constructor stub
	}

	@Autowired
	IProjectMasterService projectMasterService;
	
	@Autowired
    public EmailServiceImpl emailService;
	
    @Autowired
    ActiveUserStore activeUserStore;	

    @Value("${attachment.invoice}")
    private String attachmentPath;

    @Autowired(required = false)
    public SimpleMailMessage template;
    
	@Autowired
	UserAccountRepository userAccountRepository;
    
	@Autowired
	private ChatMasterRepository chatMasterRepository;
	
    private static final Map<String, Map<String, String>> labels;
    

    static {
        labels = new HashMap<>();

        //Simple email
        Map<String, String> props = new HashMap<>();
        props.put("headerText", "Send Simple Email");
        props.put("messageLabel", "Message");
        props.put("additionalInfo", "");
        labels.put("send", props);

        //Email with template
        props = new HashMap<>();
        props.put("headerText", "Send Email Using Template");
        props.put("messageLabel", "Template Parameter");
        props.put("additionalInfo",
                "The parameter value will be added to the following message template:<br>" +
                        "<b>This is the test email template for your email:<br>'Template Parameter'</b>"
        );
        labels.put("sendTemplate", props);

        //Email with attachment
        props = new HashMap<>();
        props.put("headerText", "Send Email With Attachment");
        props.put("messageLabel", "Message");
        props.put("additionalInfo", "To make sure that you send an attachment with this email, change the value for the 'attachment.invoice' in the application.properties file to the path to the attachment.");
        labels.put("sendAttachment", props);
    }

//    @RequestMapping(value = {"/send", "/sendTemplate", "/sendAttachment"}, method = RequestMethod.GET)
//    public String createMail(Model model,
//                             HttpServletRequest request) {
//        String action = request.getRequestURL().substring(
//                request.getRequestURL().lastIndexOf("/") + 1
//        );
//        System.out.println("in the controller");
//        Map<String, String> props = labels.get(action);
//        Set<String> keys = props.keySet();
//        Iterator<String> iterator = keys.iterator();
//        while (iterator.hasNext()) {
//            String key = iterator.next();
//            model.addAttribute(key, props.get(key));
//        }
//
//        model.addAttribute("mailObject", new MailObject());
//        return "mail/send";
//    }

    @GetMapping("/send")
    public String greetingForm(Model model) {
        model.addAttribute("mailObject", new MailObject());
        return "mail/send";
    }
    
    //@Async annotation added so calling code doesn't have to wait for the send operation to complete in order to continue
    @Async
    @PostMapping("/send")
    @ResponseBody
    public String createMail(ModelAndView model,
    						@RequestBody @Valid MailObject mailObject,
                             Errors errors) throws UnsupportedEncodingException, InterruptedException {
        if (errors.hasErrors()) {
        	for (FieldError fe:errors.getFieldErrors())
        		System.out.println("error is: "+fe.toString());
        		model.addObject("message", "failure");
        }
        emailService.sendSimpleMessage(mailObject.getTo(),mailObject.getSubject(), mailObject.getText());
        System.out.println("the mail has been sent");
        model.addObject("mailStatus", "{\"msg\":\"success\"}");
        model.setViewName("mail/send");
//        return "https://localhost:8444/search?searchLawyer=Reims&domainSelect=divorce";
//        return ("redirect:https://localhost:8444/search?searchLawyer=Reims&domainSelect=divorce");
//        return model;
        return "{\"msg\":\"success\"}";

    }

    @RequestMapping(value = "/sendTemplate", method = RequestMethod.POST)
    public String createMailWithTemplate(Model model,
                             @ModelAttribute("mailObject") @Valid MailObject mailObject,
                             Errors errors) {
        if (errors.hasErrors()) {
            return "mail/send";
        }
        emailService.sendSimpleMessageUsingTemplate(mailObject.getTo(),
                mailObject.getSubject(),
                template,
                mailObject.getText());

        return "redirect:/home";
    }

    
    @Async
    @RequestMapping(value = "/sendAttachment", method = RequestMethod.POST)
    public ModelAndView createMailWithAttachment(ModelAndView model, @RequestParam("personID") String personID,
                             @ModelAttribute @Valid MailObject mailObject, BindingResult bindingResult,
                             Errors errors) {
	    UserAccount userMaster = userAccountRepository.findOne(personID);    	
    	System.out.println("logged in users "+activeUserStore.getUsers());

	    
		//check for the uploaded file and see if it is working
		MultipartFile file = mailObject.getFile();
	    String name = file.getOriginalFilename();
        mailObject.setTo("ankujarora@gmail.com");	
        mailObject.setSubject("test");
	    try {
	      Optional<GridFSDBFile> existing = super.maybeLoadFile(name);
	      if (existing.isPresent()) {
	        super.gridFsTemplate.delete(super.getFilenameQuery(name));
	      }
//	      gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).save();
          // Get the file and save it somewhere
          byte[] bytes = file.getBytes();
          Path path = Paths.get("/Users/ankujarora1/git/ClegalServices/D:/DEV/logs/" + file.getOriginalFilename());
          Files.write(path, bytes);

         System.out.println("file successuploaded: "+ file.getOriginalFilename() + "'");
         //create a temporary project and add this message to the unread chat message history for the lawyer
//         	ProjectMaster tempProjectMaster = new ProjectMaster();
         	//find buMemberID from lawyer email
//         	tempProjectMaster.setBuMemberID(personID);
         	//create temporary userID for end user         	
//         	tempProjectMaster.setEndUserID(endUserID);
         	
         	ArrayList<ChatMessage> chatArray = new ArrayList<ChatMessage>();
         	chatArray.add(new ChatMessage(mailObject.getSenderName(), personID, mailObject.getText()));
			ChatMaster chatMaster = new ChatMaster();
	    	List<String> loggedInUsers = activeUserStore.getUsers();
	    	if(loggedInUsers.contains(userMaster.getUsername())){chatMaster.setChatArray(chatArray);}
	    	else{chatMaster.setUnreadMessages(chatArray);}
			chatMasterRepository.save(chatMaster);
//			projectMasterService.save(tempProjectMaster);
			System.out.println("message content: "+mailObject.getText());
	        attachmentPath = "/Users/ankujarora1/git/ClegalServices/D:/DEV/logs/"+ name;
//	        emailService.sendMessageWithAttachment(
//	                mailObject.getTo(),
//	                mailObject.getSubject(),
//	                mailObject.getText(),
//	                attachmentPath
//	        );
	    } catch (IOException e) {
		      System.out.println("file is not uploaded");
	    }
    	
        if (errors.hasErrors()) {
        	System.out.println("errors.getAllErrors() "+errors.getAllErrors());
    		model.addObject("userAccount", userMaster);
    		model.addObject("mailObject", new MailObject());
    		model.setViewName("/readMore");
        }
        return model;
    }
} 