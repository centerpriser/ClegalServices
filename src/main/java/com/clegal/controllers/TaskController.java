package com.clegal.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileSystemUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.clegal.model.TaskMaster;
import com.clegal.service.ITaskMasterService;
import com.clegal.service.StorageService;
import com.clegal.util.FileUpload;
import com.mongodb.gridfs.GridFSDBFile;

/**Adding tasks in the project creation forms*/
@Controller
public class TaskController extends FileUpload {
	
	@Autowired private StorageService storageService;
	@Autowired ITaskMasterService taskMasterService;
	
	@Value("${upload.files.directory}") 
	private String uploadFileDirectory;
	
	
	
	List<String> files = new ArrayList<String>();
	 
    @Autowired
    public TaskController(GridFsTemplate gridFsTemplate){
    	super(gridFsTemplate);
    }
    
	@RequestMapping(value = "/manageTask", method = RequestMethod.GET)
	public ModelAndView manageTask() {
		ModelAndView model = new ModelAndView();
		model.addObject("taskMasterList", taskMasterService.findallTask());
		model.setViewName("eadmin/manageTask");
		return model;
	}
	
	@RequestMapping(value = "/eadmin/manageTask/{id}", method = RequestMethod.GET)
	public ModelAndView editTask(@PathVariable("id") String id) {
		TaskMaster taskMaster = taskMasterService.getTaskById(id);
		ModelAndView modelView = new ModelAndView();
		modelView.addObject("taskMasterList", taskMasterService.findallTask());
		modelView.addObject("taskMaster", taskMaster);
		modelView.setViewName("eadmin/manageTask");
		return modelView;
	}
	
	@GetMapping("/listAllUploadFile")
	public String listUploadedFiles(Model model) {
		return "eadmin/manageTask";
	}
 
	@GetMapping("/eadmin/deleteTask/{id}")
	public String deleteTask(@PathVariable("id") String id,Model model) {
			TaskMaster taskMaster = taskMasterService.getTaskById(id);
			for (String fileID:taskMaster.getUploadFileIDS()){
		  		GridFSDBFile uploadedFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(fileID)));
			storageService.deleteFile(uploadedFile.getFilename());
		}
		taskMasterService.deleteTask(id);
		model.addAttribute("message", "Successfully Done!");
		return "redirect:/manageTask";
	}
	
	@PostMapping("/manageTask")
	public ModelAndView handleFileUpload(@Valid @ModelAttribute TaskMaster taskMaster, BindingResult result,
			@RequestParam("file") MultipartFile file, ModelAndView model) {
			
		if(null == taskMaster.getId()){
			try {
				storageService.store(file);
//				taskMaster.setUploadFilePath(uploadFileDirectory);
//				taskMaster.setUploadFilePath("/Users/ankujarora1/git/ClegalServices/src");
//				taskMaster.setFileName(file.getOriginalFilename());
				taskMasterService.save(taskMaster);
				model.addObject("taskMasterList", taskMasterService.findallTask());
				model.addObject("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
				files.add(file.getOriginalFilename());
			} catch (Exception e) {
				model.addObject("message", "FAIL to upload " + file.getOriginalFilename() + "!");
			}
		}
		else{
			if(null!=file)
			{
				Resource resource = storageService.loadFile(uploadFileDirectory+"\\"+file.getOriginalFilename());
				try {
					FileSystemUtils.deleteRecursively(resource.getFile());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			storageService.store(file);
//			taskMaster.setFileName(file.getOriginalFilename());
			taskMasterService.updateTask(taskMaster);
		}
			
		model.setViewName("eadmin/manageTask");
		return model;
	}
 
	@GetMapping("/gellallfiles")
	public String getListFiles(Model model) {
		model.addAttribute("files",
				files.stream()
						.map(fileName -> MvcUriComponentsBuilder
								.fromMethodName(TaskController.class, "getFile", fileName).build().toString())
						.collect(Collectors.toList()));
		model.addAttribute("totalFiles", "TotalFiles: " + files.size());
		return "listFiles";
	}
 
	@GetMapping("/eadmin/viewFileById/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = storageService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	
}
