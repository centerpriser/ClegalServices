package com.clegal.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.clegal.model.MailObject;
import com.clegal.repository.MailboxRepository;
import com.clegal.service.MailboxService;

/**Controller for links that merely do forwarding to pages without any logic*/
@Controller
public class StaticLinkController {

	private static final int INITIAL_PAGE = 0;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final int BUTTONS_TO_SHOW = 5;
	
	@Value("#{'${data.domainList}'.split(',')}")
	private List<String> dataDomainList;

	@Autowired
	MailboxService mailboxService;
	
	@Autowired
	MailboxRepository mailboxRepository;
	
	@RequestMapping("/eadmin")
	public String eadminindex(Model model) {
		return "eadmin/index";
	}

	/** Login form. */
	@RequestMapping("/login.html")
	public String login() {
		return "login.html";
	}

	@RequestMapping("/index.html")
	public ModelAndView indexPage() {
		ModelAndView model = new ModelAndView();
		System.out.println("DataDomainList:>"+dataDomainList);
		model.addObject("dataDominList", dataDomainList);
		model.setViewName("/index");
		return model;
	}


	/** Administration zone index. */
	@RequestMapping("/eadmin/login.html")
	public String adminLogin() {
		return "eadmin/eadminlogin";
	}

	@RequestMapping("/about")
	public ModelAndView about() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("/about");
		 return model;
	}


	@RequestMapping("/services")
	public ModelAndView services() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("/services");
		 return model;
	}


	@RequestMapping("/blog-sidebar.html")
	public ModelAndView blogSideBar() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("/blog-sidebar");
		 return model;
	}

	@RequestMapping("/contact.html")
	public ModelAndView contact() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("/contact");
		 return model;
	}

	@RequestMapping("/lawyerDashboard")
	public ModelAndView lawyerDashboard() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("eadmin/lawyer-dashboard");
		return model;
	}



	@RequestMapping("/errorpage")
	public ModelAndView customError() {
	    ModelAndView model = new ModelAndView();
	    String errorMessage= "You are not authorized for the requested data.";
	    model.addObject("errorMsg", errorMessage);
	    model.setViewName("404");
		return model;
        }

	//	onlclick of button for video call
	@RequestMapping("/videoCall")
	public ModelAndView videoCall() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("/videoCall");
		return model;
	}
	
	//	onlclick of button for scheduling timer
	@RequestMapping("/timer")
	public ModelAndView scheduleTimer() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("eadmin/timer");
		return model;
	}
	

	
	//	onlclick of button for scheduling timer
	@RequestMapping("/compose")
	public ModelAndView composeMail() {
		System.out.println("###here");
		 ModelAndView model = new ModelAndView();
		 model.addObject("mailObject", new MailObject());
		 model.setViewName("eadmin/compose");
		return model;
	}

	//	onlclick of button for scheduling timer
	@RequestMapping("/readMail")
	public ModelAndView readMail() {
		System.out.println("###here");
		 ModelAndView model = new ModelAndView();
		 model.setViewName("eadmin/readMail");
		return model;
	}	
	
	//	allow client to see timeline view of project in progress
	@RequestMapping("/timelineView")
	public ModelAndView timelineView() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("timeline");
		return model;
	}
	
	//	allow client to see privacy policy
	@RequestMapping("/privacyPolicy")
	public ModelAndView privacyPolicy() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("privacyPolicy"
		 		+ "");
		return model;
	}
	
	//	allow client to see terms and conditions page
	@RequestMapping("/termsConditions")
	public ModelAndView termsConditions() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("termsConditions");
		return model;
	}
	
	//	allow client to see cookie policy page
	@RequestMapping("/cookiePolicy")
	public ModelAndView cookiePolicy() {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("cookiePolicy");
		return model;
	}	
}
