package com.clegal.controllers;


import com.clegal.model.OutlookEvent;
import com.clegal.model.ProjectMaster;
import com.clegal.service.CalendarService;
import com.clegal.service.ProjectMasterSevice;
import com.outlook.dev.auth.AuthHelper;
import com.outlook.dev.auth.IdToken;
import com.outlook.dev.auth.TokenResponse;
import com.outlook.dev.service.OutlookService;
import com.outlook.dev.service.OutlookServiceBuilder;
import com.outlook.dev.service.OutlookUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
public class CalendarController {

    @Autowired
    private ProjectMasterSevice projectMasterSevice;

    @Autowired
    private CalendarService calendarService;

    @RequestMapping(value = "/calendar", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {

        System.out.println("k ho yesto");

        ModelAndView model = new ModelAndView();

        UUID state = UUID.randomUUID();
        UUID nonce = UUID.randomUUID();

        // Save the state and nonce in the session so we can
        // verify after the auth process redirects back
        HttpSession session = request.getSession();
        session.setAttribute("expected_state", state);
        session.setAttribute("expected_nonce", nonce);

        String loginUrl = AuthHelper.getLoginUrl(state, nonce);

        model.addObject("syncUrl", loginUrl);

        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List<ProjectMaster> projectMasters = projectMasterSevice.findallProjectByID(loggedInUser);

        Iterable<OutlookEvent> outlookEvents = calendarService.findAllByUsername(loggedInUser);

        model.addObject("outlookEvents", outlookEvents);
//        Map<String, ProjectMaster> mappedProjectMasters = new HashMap<>();
//
//        List<TaskMaster> taskMasters = new ArrayList<>();
//
//        for (ProjectMaster projectMaster : projectMasters) {
//            taskMasters.addAll(projectMaster.getTaskMaster());
//            mappedProjectMasters.put(projectMaster.getId(), projectMaster);
//        }
//
//        model.addObject("mappedProjects", mappedProjectMasters);
//        model.addObject("tasks", taskMasters);

        model.addObject("projects", projectMasters);

        model.setViewName("eadmin/calendar");

        return model;
    }


    @RequestMapping(value = "/syncevents", method= RequestMethod.POST)
    public String authorizeAndSync (
            @RequestParam("code") String code,
            @RequestParam("id_token") String idToken,
            @RequestParam("state") UUID state,
            HttpServletRequest request) {

        System.out.println("k ho lamo jasto");
        // Get the expected state value from the session
        HttpSession session = request.getSession();
        UUID expectedState = (UUID) session.getAttribute("expected_state");
        UUID expectedNonce = (UUID) session.getAttribute("expected_nonce");
        session.removeAttribute("expected_state");
        session.removeAttribute("expected_nonce");

        // Make sure that the state query parameter returned matches
        // the expected state
        if (state.equals(expectedState)) {
            IdToken idTokenObj = IdToken.parseEncodedToken(idToken, expectedNonce.toString());
            if (idTokenObj != null) {
                TokenResponse tokenResponse = AuthHelper.getTokenFromAuthCode(code, idTokenObj.getTenantId());
                session.setAttribute("tokens", tokenResponse);
                session.setAttribute("userConnected", true);
                session.setAttribute("userName", idTokenObj.getName());
                session.setAttribute("userTenantId", idTokenObj.getTenantId());
                // Get user info
                OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokenResponse.getAccessToken(), null);
                OutlookUser user;
                try {
                    user = outlookService.getCurrentUser().execute().body();
                    session.setAttribute("userEmail", user.getMail());
                    calendarService.syncOutlookEvents(user, tokenResponse);
                } catch (IOException e) {
                    session.setAttribute("error", e.getMessage());
                    System.out.println("error " + e.getMessage());
                }
            } else {
                session.setAttribute("error", "ID token failed validation.");
            }
        } else {
            session.setAttribute("error", "Unexpected state returned from authority.");
        }



        return "redirect:/calendar";
    }
}
