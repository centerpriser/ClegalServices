package com.clegal.controllers;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
/**Security based setting*/
public class MyAppAuditor implements AuditorAware<String> {
	@Override
	public String getCurrentAuditor() {
		String result ;
		if(null != SecurityContextHolder.getContext().getAuthentication())
		{
			System.out.println("SecurityContextHolder.getContext().getAuthentication().getPrincipal()"+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
			//result = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            result = auth.getName();
			
		}
		else
		{
			result = "Ankuj Arora";
		}
		return result;
		
	}

}
