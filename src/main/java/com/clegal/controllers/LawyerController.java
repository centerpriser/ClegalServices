package com.clegal.controllers;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.LinkedInProfile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.clegal.model.ContactRequestMaster;
import com.clegal.model.MailObject;
import com.clegal.model.Mailbox;
import com.clegal.model.ProjectMaster;
import com.clegal.model.TaskMaster;
import com.clegal.model.UserAccount;
import com.clegal.repository.ChatMasterRepository;
import com.clegal.repository.ContactRequestMasterRepository;
import com.clegal.repository.MailboxRepository;
import com.clegal.repository.UserAccountRepository;
import com.clegal.repository.UsermasterMongoRepository;
import com.clegal.service.IProjectMasterService;
import com.clegal.service.ITaskMasterService;
import com.clegal.wrapper.LawyerDetailForm;
import com.clegal.wrapper.Pager;
import com.google.maps.errors.ApiException;
/**Controller for all dashboard related activities*/
@Controller
public class LawyerController  {
	
	@Autowired ITaskMasterService taskMasterService;
	
	@Autowired IProjectMasterService projectMasterService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	UsermasterMongoRepository userMasterRepository;

	@Autowired
	UserAccountRepository userAccountRepository;
	
	@Autowired
	MailboxRepository mailboxRepository;
	
	@Autowired
	ChatMasterRepository chatMasterRepository;
	
	@Autowired
	ContactRequestMasterRepository contactRequestMasterRepository;	
	
	private static final int INITIAL_PAGE = 0;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final int BUTTONS_TO_SHOW = 5;
	
	private static final Logger logger = LoggerFactory.getLogger(LawyerController.class);
	
	private LinkedIn linkedin;
	@Inject
	public LawyerController(LinkedIn linkedin) {
	this.linkedin = linkedin;
	}	
	
//	@RequestMapping(value = "/eadmin/circlepackingexpand", method = RequestMethod.GET)
//	public ModelAndView clientDashboad() {
//		ModelAndView model = new ModelAndView();
////		model.setViewName("eadmin/client-dashboard");
//		model.setViewName("circlePackingExpand");
//		return model;
//	}

	
//	@RequestMapping(value = "/readMore/{id}", method = RequestMethod.GET)
//	public ModelAndView readMore(@PathVariable("id") String id) throws ApiException, InterruptedException, IOException {
////		GeoApiContext context = new GeoApiContext.Builder()
////			    .apiKey("AIzaSyAmb8TB2X7vahf1Ky4hYUJXtcd6LJuP3rs")
////			    .build();
////			GeocodingResult[] results =  GeocodingApi.geocode(context, "30 place louis jouvet, grenoble 38000").await();
////			Gson gson = new GsonBuilder().setPrettyPrinting().create();
////			    System.out.println(gson.toJson(results[0].geometry.location));
//	    UserMaster userMaster = userMasterRepository.findOne(id);
//	    System.out.println("the name of the lawyer being searched for is"+userMaster.getName());
//		ModelAndView model = new ModelAndView();
////		model.addObject("location", results[0].geometry.location);
//		model.addObject("lawyerData", userMaster);
//		model.addObject("mailObject", new MailObject());
//		model.setViewName("/readMore");
//		return model;
//	}
	
	@RequestMapping(value = "/readMore", method = RequestMethod.POST)
	public ModelAndView readMore(@RequestParam(value="personID") String personID, HttpServletRequest request, CsrfToken token,
			HttpServletResponse response, @CookieValue(value="helplicit", required=false) String traCookie) throws ApiException, InterruptedException, IOException {
	    UserAccount userAccount = userAccountRepository.findOne(personID);
	    String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    if(traCookie==null){
			Cookie cookie = new Cookie("helplicit", Long.toString(timestamp.getTime()) + ":" + request.getRemoteAddr());
			cookie.setMaxAge(10 * 365 * 24 * 60 * 60); //set cookie for 10 years
			response.addCookie(cookie); //add cookie to response
	    }
	    else{
	    	System.out.println("traCookie## "+traCookie);
	    }	    
		ModelAndView model = new ModelAndView();
		//Add lawyer address to the model, comment temporarily to avoid API calls
//		GeoApiContext context = new GeoApiContext.Builder()
//			    .apiKey("AIzaSyB2L8Eo8vF17Ky73VT3FacrSnSgUGUK9D8")
//			    .build();
//			GeocodingResult[] results =  GeocodingApi.geocode(context, userAccount.getAddress()).await();
//		model.addObject("latitude", results[0].geometry.location.lat);
//		model.addObject("longitude", results[0].geometry.location.lng);
		System.out.println("loggedInUser## "+loggedInUser);		
		if(loggedInUser!="anonymousUser"){
			model.addObject("loggedInUser", loggedInUser);
		}
		model.addObject("userAccount", userAccount);
		model.addObject("traCookie", traCookie);
		model.addObject("mailObject", new MailObject());
//		model.addObject("token", token);
//		System.out.println("token "+token.getToken());
		model.setViewName("/readMore");
		return model;
	}	
	
//	@RequestMapping(value = "/eadmin/chat/{id}", method = RequestMethod.GET)
//	public ModelAndView clientChat(@PathVariable("id") String id, @Valid @ModelAttribute ProjectCreationForm projectCreationForm) {
//		ModelAndView model = new ModelAndView();
//		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
//		model.addObject("username", loggedInUser);
//		model.addObject("projectID", id);
//		model.setViewName("eadmin/chat");
//		String buMemberID = (projectMasterService.getProjectById(id)).getBuMemberID();
//		//find ID of the collaborator in the project and add to model
//		if(loggedInUser.equals(buMemberID)){
//			System.out.println("in the for loop");
//			model.addObject("collaborator", projectMasterService.getProjectById(id).getEndUserID());
//		}
//		else
//			model.addObject("collaborator", buMemberID);
//		ChatMaster chatMaster = chatMasterRepository.findOne((projectMasterService.getProjectById(id)).getchatMaster().getId());
//		model.addObject("chatHistory", chatMaster.getChatArray());
//		return model;
//	}	

	
	@PostMapping(value = "/unregisteredUserDashboard")
	public ModelAndView clientNotification(@RequestParam(value="cookieID") String cookieID) {
		ModelAndView model = new ModelAndView();
		model.setViewName("eadmin/chat");
		ArrayList<ContactRequestMaster> chatMessages = contactRequestMasterRepository.findChatByCookieID(cookieID);
		model.addObject("chatHistory", chatMessages);
		model.addObject("username", chatMessages.get(0).getEuID());
		model.addObject("enduser", true);
		model.addObject("unauthenticatedUser", true);
		return model;
	}
	
	@GetMapping(value = "/eadmin/chat")
	public ModelAndView clientChat() {
		ModelAndView model = new ModelAndView();
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("logged in user is "+loggedInUser);
		model.addObject("username", loggedInUser);
		model.addObject("unauthenticatedUser", false);
		model.setViewName("eadmin/chat");
		ArrayList<ContactRequestMaster> chatMessages = contactRequestMasterRepository.findByUserID(loggedInUser);
		model.addObject("chatHistory", chatMessages);
		return model;
	}
	
	@GetMapping(value = "/eadmin/profilePage")
	public ModelAndView profilePage() {
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		UserAccount userAccount = userAccountRepository.findByLawyerName(loggedInUser);
		//add the user account details to the lawyerDetailForm object
		LawyerDetailForm lawyerDetailForm = new LawyerDetailForm();
		ModelAndView model = new ModelAndView();
		model.addObject("userName", SecurityContextHolder.getContext().getAuthentication().getName());
		model.addObject("lawyerDetailForm", lawyerDetailForm);
		model.setViewName("eadmin/profilePage");
		return model;
	}
	
	@RequestMapping(value = "/eadmin/circlepackingexpand/{id}", method = RequestMethod.GET)
	public ModelAndView clientDashboard(@PathVariable("id") String id) {
		JSONObject obj, objChild, objParent;
		JSONArray jsonArray = new JSONArray();
		
		TaskMaster taskMaster = taskMasterService.getTaskById(id);
		ModelAndView modelView = new ModelAndView();
		modelView.addObject("taskMasterList", taskMasterService.findallTask());
		modelView.addObject("taskMaster", taskMaster);
		modelView.setViewName("eadmin/circlePackingExpand");
		ProjectMaster projectMaster = projectMasterService.getProjectById(id);
		ArrayList<TaskMaster> taskMasterList = new ArrayList<>();
		for (TaskMaster tm:projectMaster.getTaskMaster())
			taskMasterList.add(taskMasterService.getTaskById(tm.getId()));
		modelView.addObject("taskMasterList", taskMasterList);
		modelView.addObject("projectMaster", projectMaster);
		//put all the project and task info into a JSONObject
		
		
		
//		JSONObject innermost = new JSONObject();
//		JSONObject innermost2 = new JSONObject();
//		JSONArray innerArray = new JSONArray();
//		JSONArray innerArray2 = new JSONArray();
//		
//		innermost.put("name", "test");
//		innermost.put("ID", "1.1.1.30");
//		innermost.put("size", 700);
//		
//		innermost2.put("name", "test");
//		innermost2.put("ID", "4.1.5");
//		innermost2.put("size", 739);

		ArrayList<String> IDList = new ArrayList<String>(Arrays.asList("1.1.1.30", "4.1.5", "2.2.13"));
		
		
//		innermost.put("parent", "t1");
		JSONArray middleArray = new JSONArray();
		int counter = 0;
		for (TaskMaster tm:taskMasterList){
			
			JSONObject innerMostObject = new JSONObject();
			JSONArray innerMostArray = new JSONArray();
			JSONObject middleObject = new JSONObject();
			
			innerMostObject.put("size", 700);
			innerMostObject.put("name", tm.gettName());
			innerMostObject.put("ID", IDList.get(counter));
//			innerMostObject.put("ID", "1.1.1.30");			
			innerMostArray.add(innerMostObject);
			middleObject.put("name", tm.gettName());
			middleObject.put("children", innerMostArray);
			middleArray.add(middleObject);
			counter = counter + 1;
		}
		
		JSONObject outerobject = new JSONObject();
		outerobject.put("name", projectMaster.getProjectName());
		outerobject.put("children", middleArray);
		
		
//		innermost.put("name", "test");
//		innermost.put("ID", "1.1.1.30");
//		innermost.put("size", 700);
//		
//		innermost2.put("name", "test");
//		innermost2.put("ID", "4.1.5");
//		innermost2.put("size", 739);
//		
//		
//		
////		innermost.put("parent", "t1");
//		
//		
//		innerArray.add(innermost);
//		innerArray2.add(innermost2);
//		
//		JSONObject middleobject = new JSONObject();
//		JSONObject middleobject2 = new JSONObject();
//		
//		JSONArray middlearray = new JSONArray();
//		middleobject.put("name", "t1");
//		middleobject.put("children", innerArray);
//		
//		middleobject2.put("name", "t2");
//		middleobject2.put("children", innerArray2);
//		
//		middlearray.add(middleobject);
//		
//		middlearray.add(middleobject2);
//		
//		
//		
//		JSONObject outerobject = new JSONObject();
//		JSONArray outerarray = new JSONArray();
//		outerobject.put("name", "p1");
//		outerobject.put("children", middlearray);
		
		
//		objParent = new JSONObject();
//		objParent.put("name", projectMaster.getProjectName());
//		objParent.put("parent", "");
//		objParent.put("ID", "");
//		objParent.put("size", "");
//		jsonArray.add(objParent);
//		
//		
//		JSONObject jsn = new JSONObject(); 
//		jsn.put("name", "temp");
//		jsn.put("parent", projectMaster.getProjectName());
//		jsn.put("ID", "");
//		jsn.put("size", "");
//		jsonArray.add(jsn);
//		
//		for (TaskMaster tm:taskMasterList){
//			JSONArray jsonArrayInner = new JSONArray();
//			obj = new JSONObject();
//			objChild = new JSONObject();
//			obj.put("name", tm.gettName());
//			obj.put("parent", projectMaster.getProjectName());
//			obj.put("ID", "");
//			obj.put("size", "");
//			objChild.put("parent", tm.gettName());
//			objChild.put("name", "test");
//			objChild.put("ID", "1.1.1.30");
//			objChild.put("size", 700);
//			jsonArray.add(obj);
//			jsonArray.add(objChild);
////			jsonArrayInner.add(objChild);
////			jsonArray.add(jsonArrayInner);
//		}
		System.out.println("jsonarray: "+outerobject);
		modelView.addObject("jsonArray", outerobject);
		
		
		
		return modelView;
	}
	
	
	//	code to access LinkedIn account of user
	@GetMapping(value = "/linkedin")
	public String helloLinkedIn(Model model) {
		try {
		if (!linkedin.isAuthorized()) {
		return "redirect:/connect/linkedin";
	
		}}catch (NullPointerException e){
		return "redirect:/connect/linkedin";
		}
	
		LinkedInProfile profile = linkedin.profileOperations().getUserProfile();
		String profileId = linkedin.profileOperations().getProfileId();
		System.out.println("profile.getFirstName() "+profile.getLastName());
		return "eadmin/index";
	}	
	
	//	code to open up the gmail connection form for the lawyer
	@GetMapping(value = "/mailboxConfig")
	public String mailboxConfig(Model model) {
		logger.debug("in the mailboxConfig method");
		model.addAttribute("userAccount", new UserAccount());
		return "eadmin/mailboxConfig";
	}
	
	//	access mailbox
	@RequestMapping("/mailbox")
	public ModelAndView mailbox(@RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page, @RequestParam("username") String username
			, @RequestParam("password") String password, Pageable pageable, ModelAndView model, BindingResult bindingResult) throws MessagingException {
		logger.debug("in the mailbox method");
		if(username.isEmpty() || password.isEmpty()){
//			FieldError fieldError = new FieldError("userAccount", "username", messageSource.getMessage("user.exists", null, LocaleContextHolder.getLocale()));
//			bindingResult.addError(fieldError);
//			if(bindingResult.hasErrors()){
//				System.out.println("errors "+bindingResult.getAllErrors());
//			}
			model.addObject("userMailboxCredentialsNotNull", messageSource.getMessage("notnull.userMailboxCredentials", null, LocaleContextHolder.getLocale()));
			model.setViewName("eadmin/mailboxConfig");
			return model;
		}
//        // 1. Setup properties for the mail session. Sorround with try/catch block and onerror send back to mailboxConfig page with error message.
//        Properties props = new Properties();
//        props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.pop3.socketFactory.fallback", "false");
//        props.put("mail.pop3.socketFactory.port", "995");
//        props.put("mail.pop3.port", "995");
//        props.put("mail.pop3.host", "imap.gmail.com");
//        props.put("mail.pop3.user", "ankujarora");
//        props.put("mail.store.protocol", "imaps");
//
//        // 2. Creates a javax.mail.Authenticator object.
//        Authenticator auth = new Authenticator() {
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication("ankujarora", "keepout21!");
//            }
//        };
//
//        // 3. Creating mail session.
//        Session session = Session.getDefaultInstance(props, auth);
//
//        // 4. Get the POP3 store provider and connect to the store.
//        Store store = session.getStore("imaps");
//        store.connect("pop.gmail.com", "ankujarora", "keepout21!");
//
//        
//        //in the meanwhile, get the names of all the folders
//        Folder[] folder = store.getDefaultFolder().list(); // get the array folders in server
//        int i=0;
//        for( i=0;i<folder.length;i++)
//        System.out.println("Press "+i+" to read "+folder[i].getName()+" folder");
//        
//        // 5. Get folder and open the INBOX folder in the store.
//        Folder inbox = store.getFolder("misc");
//        inbox.open(Folder.READ_ONLY);
//
//        // 6. Retrieve the messages from the folder.
//        Message[] messages = inbox.getMessages();
//        ArrayList<String> subjects = new ArrayList<>();
//        for (Message message : messages) {
//            Mailbox mailbox = new Mailbox();
//        	mailbox.setSubject(message.getSubject());
//            mailboxService.save(mailbox);
//        	subjects.add(message.getSubject());
//        	Flags flag = message.getFlags();
//        	String[] flag1 = flag.getUserFlags();
//        	for (String flag2:flag1){
//        		System.out.println("flag2 "+flag2);
//        	}
//        
//        	if (message.getFlags().contains(Flag.SEEN)){
//        		System.out.println("message "+message.getSubject());
//        	}
//
////            message.writeTo(System.out);
//        }
//        // 7. Close folder and close store.
//        inbox.close(false);
//        store.close();
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		pageable=new PageRequest(evalPage, 10);
		Page<Mailbox> usermaster = mailboxRepository.findAllMails("r", pageable);
		model.addObject("persons", usermaster);
		model.addObject("pageSizes", PAGE_SIZES);
		System.out.println("usermaster.getTotalPages() "+usermaster.getNumberOfElements());
		Pager pager = new Pager(usermaster.getTotalPages(), usermaster.getNumber(), BUTTONS_TO_SHOW);
		model.addObject("pager", pager);
		model.addObject("selectedPageSize", 10);
		model.addObject("username", username);
		model.addObject("password", password);
		 model.setViewName("eadmin/mailbox");
		return model;
	}		
	//process registration form
	// Process form input data
/*	@RequestMapping(value = "/lawyerDetails", method = RequestMethod.POST)
	public String lawyerDetailsForm(ModelAndView modelAndView, @Valid @ModelAttribute UserAccount userAccount, BindingResult bindingResult, HttpServletRequest request,
			ModelMap model, RedirectAttributes redirectAttributes) {
			
		// Lookup user in database by e-mail
		UserAccount userExists = userAccountRepository.findByEmail(userAccount.getEmail());
		if (userExists != null) {
			System.out.println("user exists");
//			modelAndView.addObject("error", "Please correct the form errors");
//			modelAndView.setViewName("/eadmin/login");
//			bindingResult.rejectValue("email", messageSource.getMessage("user.exists", null, LocaleContextHolder.getLocale()));
			FieldError fieldError = new FieldError("userAccount", "email", messageSource.getMessage("user.exists", null, LocaleContextHolder.getLocale()));
			bindingResult.addError(fieldError);
			userAccount.setSigninError("true");
//			return "eadmin/login";
		}
		if(userAccount.getIsLawyer()){
			userAccount.addRole(userservices.getRole("ROLE_BU"));
		}else userAccount.addRole(userservices.getRole("ROLE_USER"));
		if (bindingResult.hasErrors()) {
			System.out.println("get all errors "+ bindingResult.getAllErrors());
			System.out.println("in the error loop");
//			modelAndView.addObject("signupError", "Please correct the form errors");
//			modelAndView.addObject("user", user);
//			modelAndView.setViewName("/eadmin/login");
//	        redirectAttributes.addFlashAttribute("user", user);
//	        model.addAttribute("user", user);
			userAccount.setSigninError("true");
//	          return "eadmin/login";
		} else { // new user so we create user and send confirmation e-mail
					
			
			// Disable user until they click on confirmation link in email
		    userAccount.setEnabled(false);
		      
		    // Generate random 36-character string token for confirmation link
		    userAccount.setConfirmationToken(UUID.randomUUID().toString());
		        
			userAccount.setStatus(UserAccountStatus.STATUS_PENDING.name());	
			userAccount.setEnabled(false);
			userAccount.setAccountNonExpired(true);
			userAccount.setAccountNonLocked(false);
			userAccount.setTempPassword(false);
			userAccount.setCredentialsNonExpired(true);
		    userservices.save(userAccount);
				
			String appUrl = request.getScheme() + "://" + request.getServerName();
			MailObject mailObject = new MailObject();
			mailObject.setTo(userAccount.getEmail());
			mailObject.setSubject("Registration Confirmation");
			mailObject.setText("To confirm your e-mail address, please click the link below:\n"
					+ appUrl + "/confirm?token=" + userAccount.getConfirmationToken());
			//mailObject.setFrom("noreply@domain.com");
			
			emailService.sendSimpleMessage(mailObject.getTo(), mailObject.getSubject(), mailObject.getText());
			return "redirect:/confirmationMessageNotification";
			
//			modelAndView.addObject("confirmationMessageNotification", "A confirmation e-mail has been sent to " + userAccount.getEmail());
//			modelAndView.setViewName("eadmin/login");
		}
	    return "eadmin/login";
	}	
*/
}
