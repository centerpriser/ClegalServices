package com.clegal.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
import com.clegal.model.ContactRequestMaster;
import com.clegal.model.UserAccount;
import com.clegal.repository.ChatMasterRepository;
import com.clegal.repository.ContactRequestMasterRepository;
import com.clegal.repository.UserAccountRepository;
import com.clegal.service.IChatMasterService;
import com.clegal.service.IContactRequestMasterService;
import com.clegal.service.IProjectMasterService;
import com.clegal.service.IUserService;
import com.clegal.util.FileUpload;
import com.mongodb.gridfs.GridFSDBFile;

/**
 * Created by ankujaora to handle end to end chat functionality.
 */
@Controller
public class ChatController extends FileUpload{
	
    public ChatController(GridFsTemplate gridFsTemplate) {
		super(gridFsTemplate);
		// TODO Auto-generated constructor stub
	}

	@Autowired private SimpMessagingTemplate simpMessagingTemplate;
	
	@Autowired
	ChatMasterRepository chatMasterRepository;

	@Autowired
	ContactRequestMasterRepository contactRequestMasterRepository;
	
	@Autowired
	IUserService userservices;
	
	@Autowired
	IProjectMasterService projectMasterService;	
	
	@Autowired
	IChatMasterService chatMasterService;
	
	
	@Autowired
	IContactRequestMasterService contactRequestMasterService;	
	
	@Autowired
	UserAccountRepository userAccountRepository;
	
	private Set<String> chatMembersList = new HashSet<String>();
	private Set<String> sessionIDs = new HashSet<String>();
	
    @MessageMapping("/queue/notification")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
		System.out.println("in the public method");
        return chatMessage;
    }

    @MessageMapping("/chat.addUserAnonymous.{username}")
    public void addUserAnonymous(@Payload ChatMessage message, @DestinationVariable("username") String username, SimpMessageHeaderAccessor headerAccessor) {
    	chatMembersList.add(username + "-" + headerAccessor.getSessionId());
    	headerAccessor.getSessionAttributes().put("chatMembersList", chatMembersList);
    	headerAccessor.getSessionAttributes().put("username", username);
    	System.out.println("chatMembersList "+chatMembersList);
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification", message);
//		simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/notification", message);
    }
    
    @MessageMapping("/chat.addUser")
    public void addUser(@Payload ChatMessage message, Principal principal, SimpMessageHeaderAccessor headerAccessor) {
    	String username = principal.getName();
    	chatMembersList.add(principal.getName());
    	headerAccessor.getSessionAttributes().put("chatMembersList", chatMembersList);
    	headerAccessor.getSessionAttributes().put("username", username);
    	System.out.println("chatMembersList "+chatMembersList);
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification", message);
//		simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/notification", message);
    }
    
    /*Keep this method to ensure that the lawyer can interact with a user who is not a client yet. Create a separate method for
     * when the user becomes a client and then chats with the lawyer. There should be a separate subscription channel for that.*/
	@MessageMapping("/chat.private.{username}")
	public void filterPrivateMessage(@Payload ChatMessage message, @DestinationVariable("username") String username, Principal principal, SimpMessageHeaderAccessor headerAccessor) {
//		checkProfanityAndSanitize(message);
		ContactRequestMaster chatMaster = new ContactRequestMaster();
		message.setSender(principal.getName());
//		ProjectMaster projectMaster = projectMasterService.getProjectById(message.getProjectID());
		System.out.println("message.getDocumentID() "+message.getDocumentID());
		String chatID = message.getDocumentID().trim();
		//check if one of the users is offline, and store the messages as unread ones
        Set<String> chatMembersList = (Set<String>) headerAccessor.getSessionAttributes().get("chatMembersList");
		chatMaster = contactRequestMasterService.getChatById(message.getDocumentID());
        if(chatMembersList.size()<2){
    		ArrayList<ChatMessage> unreadChatArray = chatMaster.getUnreadMessages();
    		if(unreadChatArray==null){unreadChatArray = new ArrayList<ChatMessage>();}
    		unreadChatArray.add(message);
    		chatMaster.setUnreadMessages(unreadChatArray);
    		contactRequestMasterService.updateUnreadMessages(message.getDocumentID(), unreadChatArray);    		
        }
        else{
    		ArrayList<ChatMessage> chatArray = chatMaster.getChatArray();
    		chatArray.add(message);
    		chatMaster.setChatArray(chatArray);
    		contactRequestMasterService.updateChat(message.getDocumentID(), chatArray);
        }
        //check whether the receiving entity is an unverified user, in which case the message is sent by adding the session ID at the end
        for(String sessionUser:chatMembersList){
        	if((sessionUser.contains(username))&&(sessionUser.contains("-"))){
        		simpMessagingTemplate.convertAndSend("/queue/notification/videoCallRequest-"+sessionUser.substring(sessionUser.indexOf("-") + 1, sessionUser.length()), message);
        	}
        }
        System.out.println("sending the message to*** "+username);
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification", message);
		System.out.println("sending the message back to**** "+principal.getName());
		simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/notification", message);
	}

    /*Keep this method to ensure that the lawyer can interact with a user who is not a client yet. Create a separate method for
     * when the user becomes a client and then chats with the lawyer. There should be a separate subscription channel for that.*/
	@MessageMapping("/chat.privateAn.{username}")
	public void filterPrivateMessageAn(@Payload ChatMessage message, @DestinationVariable("username") String username, SimpMessageHeaderAccessor headerAccessor) {
//		checkProfanityAndSanitize(message);
		ContactRequestMaster chatMaster = new ContactRequestMaster();
//		ProjectMaster projectMaster = projectMasterService.getProjectById(message.getProjectID());
		System.out.println("message.getDocumentID() "+message.getDocumentID());
		String chatID = message.getDocumentID().trim();
		//check if one of the users is offline, and store the messages as unread ones
        Set<String> chatMembersList = (Set<String>) headerAccessor.getSessionAttributes().get("chatMembersList");
		chatMaster = contactRequestMasterService.getChatById(message.getDocumentID());
        if(chatMembersList.size()<2){
    		ArrayList<ChatMessage> unreadChatArray = chatMaster.getUnreadMessages();
    		if(unreadChatArray==null){unreadChatArray = new ArrayList<ChatMessage>();}
    		unreadChatArray.add(message);
    		chatMaster.setUnreadMessages(unreadChatArray);
    		contactRequestMasterService.updateUnreadMessages(message.getDocumentID(), unreadChatArray);    		
        }
        else{
    		ArrayList<ChatMessage> chatArray = chatMaster.getChatArray();
    		chatArray.add(message);
    		chatMaster.setChatArray(chatArray);
    		contactRequestMasterService.updateChat(message.getDocumentID(), chatArray);
        }
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification", message);
		simpMessagingTemplate.convertAndSend("/queue/notification/videoCallRequest-"+headerAccessor.getSessionId(), message);
	}
	
	
	/*function called in case of a request sent by an unauthenticated user*/
	@MessageMapping("/chat.videoCallRequestAn")
	public void videoCallRequestAn(@Payload ChatMessage message, SimpMessageHeaderAccessor headerAccessor) {
		String sender = message.getSender();
		String username = message.getReceiver();
		//find if IP address exists in contactRequestMaster table
		ContactRequestMaster contactRequestMaster = contactRequestMasterRepository.findUserByCookieID(username, sender, message.getcookieID());
		ArrayList<ChatMessage> chatArray = new ArrayList<ChatMessage>();
		ChatMaster chatMaster = new ChatMaster();		
		//if account does not exist it must be created
		if (contactRequestMaster==null){
			contactRequestMaster = new ContactRequestMaster();
			contactRequestMaster.setBuID(username);
			contactRequestMaster.setEuID(sender);
			contactRequestMaster.setcookieID(message.getcookieID());
			contactRequestMaster.setChatArray(new ArrayList<ChatMessage>());
			chatArray.add(new ChatMessage(message.getSender(), message.getReceiver(), message.getContent()));
			contactRequestMaster.setUnreadMessages(chatArray);
			contactRequestMasterService.save(contactRequestMaster);
		}
		//user already exists
		else{
			chatArray = contactRequestMaster.getUnreadMessages();
	     	chatArray.add(new ChatMessage(message.getSender(), message.getReceiver(), message.getContent()));
	     	contactRequestMaster.setUnreadMessages(chatArray);
			contactRequestMasterService.updateChat(contactRequestMaster.getId(), chatArray);
		}
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification/videoCallRequest", message);
		simpMessagingTemplate.convertAndSend("/queue/notification/videoCallRequest-"+headerAccessor.getSessionId(), message);
	}
	
	/*function called in case of a request sent by an unauthenticated user*/	
	@MessageMapping("/chat.videoCallRequest")
	public void videoCallRequest(@Payload ChatMessage message, Principal principal, SimpMessageHeaderAccessor headerAccessor) {
		String sender = principal.getName();
		String username = message.getReceiver();
		//find if IP address exists in contactRequestMaster table
		ContactRequestMaster contactRequestMaster = contactRequestMasterRepository.findUserByCookieID(username, sender, message.getcookieID());
		ArrayList<ChatMessage> chatArray = new ArrayList<ChatMessage>();
		ChatMaster chatMaster = new ChatMaster();		
		//if account does not exist it must be created
		if (contactRequestMaster==null){
			contactRequestMaster = new ContactRequestMaster();
			contactRequestMaster.setBuID(username);
			contactRequestMaster.setEuID(sender);
			contactRequestMaster.setcookieID(message.getcookieID());
			contactRequestMaster.setChatArray(new ArrayList<ChatMessage>());
			chatArray.add(new ChatMessage(message.getSender(), message.getReceiver(), message.getContent()));
			contactRequestMaster.setUnreadMessages(chatArray);
			contactRequestMasterService.save(contactRequestMaster);
		}
		//user already exists
		else{
			chatArray = contactRequestMaster.getUnreadMessages();
	     	chatArray.add(new ChatMessage(message.getSender(), message.getReceiver(), message.getContent()));
	     	contactRequestMaster.setUnreadMessages(chatArray);
			contactRequestMasterService.updateChat(contactRequestMaster.getId(), chatArray);
		}
		simpMessagingTemplate.convertAndSendToUser(username, "/queue/notification/videoCallRequest", message);
		simpMessagingTemplate.convertAndSend("/queue/notification/videoCallRequest-"+headerAccessor.getSessionId(), message);
	}	
	
//	private void checkProfanityAndSanitize(ChatMessage message) {
//		long profanityLevel = profanityFilter.getMessageProfanity(message.getMessage());
//		profanity.increment(profanityLevel);
//		message.setMessage(profanityFilter.filter(message.getMessage()));
//	}
	
    @Async
    @RequestMapping(value = "/sendFileForVideoCall", method = RequestMethod.POST)
    public ModelAndView createMailWithAttachment(@RequestParam("upload_file") MultipartFile file, @RequestParam("personID") String personID) {
		//check for the uploaded file and see if it is working
    	System.out.println("in the method with person ID "+personID);
	    String name = file.getOriginalFilename();
	    try {
	      Optional<GridFSDBFile> existing = super.maybeLoadFile(name);
	      if (existing.isPresent()) {
	        super.gridFsTemplate.delete(super.getFilenameQuery(name));
	      }
//	      gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).save();
          // Get the file and save it somewhere
          byte[] bytes = file.getBytes();
          Path path = Paths.get("/Users/ankujarora1/git/ClegalServices/D:/DEV/logs/" + file.getOriginalFilename());
          Files.write(path, bytes);

         System.out.println("file successuploaded: "+ file.getOriginalFilename() + "'");
	    } catch (IOException e) {
		      System.out.println("file is not uploaded");
	    }
	    UserAccount userMaster = userAccountRepository.findOne(personID);
	    ModelAndView model = new ModelAndView();
	    model.addObject("userAccount", userMaster);
	    model.setViewName("readMore");
	    return model;
    }	
    
    @RequestMapping(value = "/loadSpecificChat", method = RequestMethod.POST)
    public ResponseEntity<?> loadSpecificChat(@RequestBody String id) {
    	//take out double quotes
    	id = id.substring(1, id.length()-1);
    	System.out.println("id is "+ id.substring(1, id.length()-1));
    	ContactRequestMaster chatMessages = contactRequestMasterService.getChatById(id);
    	return ResponseEntity.ok(chatMessages);
    }
    
    @PostMapping(value = "/markMessagesAsRead")
    public ResponseEntity<?> markMessagesAsRead(@RequestBody String id) {
    	ContactRequestMaster chatMessages = null;
    	try{
    		id = id.substring(1, id.length()-1);
        	System.out.println("in markMessagesAsRead, id is "+ id);
        	chatMessages = contactRequestMasterService.getChatById(id);
        	//move unread messages to read, and delete the unread message array;
        	if(chatMessages.getUnreadMessages()!=null){
            	ArrayList<ChatMessage> unreadMessages = chatMessages.getUnreadMessages();
            	chatMessages.getChatArray().addAll(unreadMessages);
            	chatMessages.setChatArray(chatMessages.getChatArray());
            	unreadMessages.clear(); 
            	contactRequestMasterService.updateUnreadMessages(id, unreadMessages);
            	contactRequestMasterService.updateChat(id, chatMessages.getChatArray());
        	}

    	}
    	catch(Exception e){
    		e.printStackTrace();
    		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    	}
    	return ResponseEntity.ok(chatMessages);
    } 
    
    
}
