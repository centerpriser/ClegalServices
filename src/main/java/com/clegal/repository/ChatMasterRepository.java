package com.clegal.repository;

import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
/**Interfact for the ChatMaster db table*/
public interface ChatMasterRepository extends CrudRepository<ChatMaster, String> {
//	@Query("{'$and':[ {'buID':?0 }, {'$gt': [{'$numberOfMessages': {'$size':'unreadMessages'}}, 0]} ] }")
	@Query("{'$and':[ {'buID':?0 }, {'chatArray.1': {$exists: true}} ] }")	
    ArrayList<ChatMessage> findUnreadNotifications(String searchLawyer);

	@Query("{'$or':[ {'buID':?0 }, {'euID':?0} ] }, {unreadMessages : 1}")
    ArrayList<ChatMaster> findByUserID(String buID);
	
}
