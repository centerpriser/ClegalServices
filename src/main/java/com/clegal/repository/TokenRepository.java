package com.clegal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.clegal.model.Token;
/**Interface for the Token db table*/
public interface TokenRepository extends MongoRepository<Token, String> {
    Token findBySeries(String series);
    Token findByUsername(String username);
}