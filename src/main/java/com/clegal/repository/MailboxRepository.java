package com.clegal.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.clegal.model.Mailbox;



public interface MailboxRepository extends CrudRepository<Mailbox, String>{

	@Query("{ subject : { $regex: ?0 } }")
    Page<Mailbox> findAllMails(String s, Pageable pageable);
}