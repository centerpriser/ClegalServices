package com.clegal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.clegal.model.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
}
