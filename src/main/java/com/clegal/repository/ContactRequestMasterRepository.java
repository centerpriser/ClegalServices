package com.clegal.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.clegal.model.ChatMaster;
import com.clegal.model.ChatMessage;
import com.clegal.model.ContactRequestMaster;
import com.clegal.model.UserAccount;
/**Interfact for the ChatMaster db table*/
public interface ContactRequestMasterRepository extends CrudRepository<ContactRequestMaster, String> {
//	@Query("{'$and':[ {'buID':?0 }, {'$gt': [{'$numberOfMessages': {'$size':'unreadMessages'}}, 0]} ] }")
	@Query("{'$and':[ {'buID':?0 }, {'chatArray.1': {$exists: true}} ] }")	
    ArrayList<ChatMessage> findUnreadNotifications(String searchLawyer);

	@Query("{'$or':[ {'buID':?0 }, {'euID':?0} ] }, {unreadMessages : 1}")
    ArrayList<ContactRequestMaster> findByUserID(String buID);

	@Query("{ '$and':[{'buID' : ?0}, {'euID' : ?1}, {'cookieID' : ?2} ] }")
	ContactRequestMaster findUserByCookieID(final String username, final String sender, final String cookieID);
	
	@Query("{'cookieID' : ?0}")
	ArrayList<ContactRequestMaster> findChatByCookieID(final String cookieID);	
	
}
