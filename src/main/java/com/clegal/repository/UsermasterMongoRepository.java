package com.clegal.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.clegal.model.UserMaster;
/**Used for the search functionality where the user needs to find the lawyer*/
//public interface UsermasterMongoRepository extends CrudRepository<UserMaster, String>{
public interface UsermasterMongoRepository extends MongoRepository<UserMaster, String>{	
	
    @Query("{address: { $regex: ?0 } })")
    Page<UserMaster> searchLawyerFromUserMaster(String searchLawyer, Pageable pageable);
    
	@Query("{ '$text' : { $search: ?0 } }")
    Page<UserMaster> findUserMasterByRegexpAddress(String searchLawyer, Pageable pageable);
 
//    Page<UserMaster> findAll(Pageable pageable);
    
    @Query("{address: { $regex: ?0, $options: 'i'} })")
	List<UserMaster> findMatchingLocations(String location);

	
	
    @Query("{ '$text' : { $search: ?0 } }")
    public Page<UserMaster> findAll(final String city, Pageable pageable);
    
   /* @Query("{ '$text' : { $search: ?0 } }")
    public Page<UserMaster> findAll
    
    

   // Page<UserMaster> findAll(person.lastname.contains("a"), new PageRequest(0, 2, Direction.ASC, "lastname"));
    
    @Override
    default <S extends UserMaster> Page<S> findAll(Example<S> example, Pageable pageable) {
    	// TODO Auto-generated method stub
    	return null;
    }*/
    
}