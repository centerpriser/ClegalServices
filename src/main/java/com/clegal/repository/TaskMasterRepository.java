package com.clegal.repository;

import org.springframework.data.repository.CrudRepository;

import com.clegal.model.TaskMaster;

public interface TaskMasterRepository extends CrudRepository<TaskMaster, String>{}