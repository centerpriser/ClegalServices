package com.clegal.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.clegal.model.UserAccount;
import com.clegal.model.UserInformation;
/**Interface to do CRUD and custom operations on the DB*/
import com.clegal.model.UserMaster;
public interface UserAccountRepository extends MongoRepository<UserAccount, String> {

	UserAccount findByUsername(final String username);

	List<UserAccount> findByUsernameAndPassword(final String username, final String password);

	List<UserAccount> findByPasswordAndUsername(final String password, final String username);

	List<UserAccount> findByUsernameLike(final String username);

	@Query("{ 'username' : ?0, 'password' : ?1 }")
	List<UserAccount> findByUsernameAndPasswordQuery(final String username, final String password);

	@Query("{ 'username' : ?0 }")
	UserAccount findByLawyerName(final String username);

	@Query("{ 'email' : ?0}")
	Optional<UserAccount> findByEmail(final String email);
	
	UserAccount findOne(String id);
	
	@Query("{ 'confirmationToken' : ?0}")
	UserAccount findByConfirmationToken(String confirmationToken);
	
	@Query("{ 'ipAddress' : ?0 }")
	UserAccount findUserByIpAddress(final String ipAddress);	

    @Query("{address: { $regex: ?0 } })")
    Page<UserAccount> searchLawyerFromUserMaster(String searchLawyer, Pageable pageable);
    
	@Query("{ '$text' : { $search: ?0 } }")
    Page<UserAccount> findUserMasterByRegexpAddress(String searchLawyer, Pageable pageable);
 
//    Page<UserMaster> findAll(Pageable pageable);
    
    @Query("{address: { $regex: ?0, $options: 'i'} })")
	List<UserAccount> findMatchingLocations(String location);

	
	
    @Query("{ '$text' : { $search: ?0 } }")
    public Page<UserAccount> findAll(final String city, Pageable pageable);

    @Query("{ 'resetToken' : ?0 }")
	Optional<UserAccount> findByResetToken(String token);
    
}
