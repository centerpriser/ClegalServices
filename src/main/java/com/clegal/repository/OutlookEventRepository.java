package com.clegal.repository;

import com.clegal.model.OutlookEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutlookEventRepository extends CrudRepository<OutlookEvent, String> {

    Iterable<OutlookEvent> findOutLookEventsByUserId(String user);

    void deleteOutLookEventsByUserId(String user);
}
