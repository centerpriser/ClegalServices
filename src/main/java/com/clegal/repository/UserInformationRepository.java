//package com.clegal.repository;
//
//import java.util.List;
//
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.data.mongodb.repository.Query;
//
//import com.clegal.model.UserInformation;
//
//public interface UserInformationRepository extends MongoRepository<UserInformation, String> {
//
//	UserInformation findByUsername(final String username);
//
//	List<UserInformation> findByUsernameAndPassword(final String username, final String password);
//
//	List<UserInformation> findByPasswordAndUsername(final String password, final String username);
//
//	List<UserInformation> findByUsernameLike(final String username);
//
//	@Query("{ 'username' : ?0, 'password' : ?1 }")
//	List<UserInformation> findByUsernameAndPasswordQuery(final String username, final String password);
//
//	UserInformation findOne(String id);
//
//	UserInformation findByConfirmationToken(String confirmationToken);
//
//}
