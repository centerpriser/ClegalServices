package com.clegal.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.clegal.model.ProjectMaster;
/**Interface for the DB table ProjectMaster*/
public interface ProjectMasterRepository extends CrudRepository<ProjectMaster, String>{
	
	@Query("{'$or':[ {'buMemberID':?0 }, {'endUserID':?0} ] }")
    List<ProjectMaster> findProjectsByUserName(String searchLawyer);

}