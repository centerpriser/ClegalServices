package com.clegal.repository;

import com.clegal.model.UserMaster;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UsermasterSearchRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	MongoOperations mongoOperation;

	public Collection<UserMaster> searchUsermaster(String text) {
		return mongoTemplate.find(
				Query.query(new Criteria().orOperator(Criteria.where("domainExperties").regex(text, "i"),
						Criteria.where("fname").regex(text, "i"),Criteria.where("email").regex(text, "i"), Criteria.where("yearofexperience").regex(text, "i"))),
				UserMaster.class);
	}

	public UserMaster deletefromUserMaster(String make, String model) {
		Query query2 = new Query();
		query2.addCriteria(Criteria.where("fname").is(make).and("email").is(model));
		UserMaster myUserMaster = mongoOperation.findOne(query2, UserMaster.class);
		return myUserMaster;
	}
	//	search lawyer with particular name
	public UserMaster searchLawyerFromUserMaster(String searchLawyer) {
		Query query2 = new Query();
//		query2.addCriteria(Criteria.where("name").regex(searchLawyer, "i"));
		query2.addCriteria(Criteria.where("name").is("Maître Caroline FLIN"));		
		UserMaster myUserMaster = mongoOperation.findOne(query2, UserMaster.class);
		return myUserMaster;
	}
	
}
